<?php

/**
 * [BEGIN_COT_EXT]
 * Hooks=standalone
 * [END_COT_EXT]
 */
/**
 * Yandex kassa billing Plugin
 *
 * @package ykbilling
 * @version 1.0
 * @author devkont (Yusupov)
 * @copyright (c) CMSWorks Team 2015
 * @license BSD
 */
defined('COT_CODE') && defined('COT_PLUG') or die('Wrong URL');

require_once cot_incfile('ykbilling', 'plug');
require_once cot_incfile('payments', 'module');

$m = cot_import('m', 'G', 'ALP');
$pid = cot_import('pid', 'G', 'INT');

if (empty($m))
{
	// Получаем информацию о заказе
	if (!empty($pid) && $pinfo = cot_payments_payinfo($pid))
	{

		cot_block($usr['id'] == $pinfo['pay_userid']);
		cot_block($pinfo['pay_status'] == 'new' || $pinfo['pay_status'] == 'process');

		$out_summ = number_format($pinfo['pay_summ']*$cfg['plugin']['ykbilling']['rate'], 2, '.', '');
		$inv_id = $pid;
		$inv_desc = $pinfo['pay_desc'];
		$inv_desc2 = $pinfo['pay_desc'];
		
		$action_url = ($cfg['plugin']['ykbilling']['testmode']) ? 'https://demomoney.yandex.ru/eshop.xml' : 'https://money.yandex.ru/eshop.xml';
		
		if($cfg['plugin']['ykbilling']['typechoise'] && $cfg['plugin']['ykbilling']['payments']){

			$paymentTypes = explode(',', $cfg['plugin']['ykbilling']['payments']);

			$payment_type_choise = "<div class=\"well\">";
			$payment_type_choise .= (in_array('AC', $paymentTypes)) ? 
			"<div class=\"media\"><label class=\"checkbox\"><input type=\"radio\" name=\"paymentType\" checked=\"checked\" value=\"AC\"> ".$L['ykbilling_paymenttype_card']."</label></div>" : "";
			
			$payment_type_choise .= (in_array('SB', $paymentTypes)) ? 
			"<div class=\"media\"><label class=\"checkbox\"><input type=\"radio\" name=\"paymentType\" value=\"SB\"> ".$L['ykbilling_paymenttype_sberbank']."</label></div>" : "";

			$payment_type_choise .= (in_array('WM', $paymentTypes)) ? 
			"<div class=\"media\"><label class=\"checkbox\"><input type=\"radio\" name=\"paymentType\" value=\"WM\"> ".$L['ykbilling_paymenttype_wmr']."</label></div>" : "";

			$payment_type_choise .= (in_array('GP', $paymentTypes)) ? 
			"<div class=\"media\"><label class=\"checkbox\"><input type=\"radio\" name=\"paymentType\" value=\"GP\"> ".$L['ykbilling_paymenttype_terminal']."</label></div>" : "";

			$payment_type_choise .= (in_array('AB', $paymentTypes)) ? 
			"<div class=\"media\"><label class=\"checkbox\"><input type=\"radio\" name=\"paymentType\" value=\"AB\"> ".$L['ykbilling_paymenttype_alfabank']."</label></div>" : "";

			$payment_type_choise .= (in_array('PB', $paymentTypes)) ? 
			"<div class=\"media\"><label class=\"checkbox\"><input type=\"radio\" name=\"paymentType\" value=\"PB\"> ".$L['ykbilling_paymenttype_psb']."</label></div>" : "";

			$payment_type_choise .= (in_array('MA', $paymentTypes)) ? 
			"<div class=\"media\"><label class=\"checkbox\"><input type=\"radio\" name=\"paymentType\" value=\"MA\"> ".$L['ykbilling_paymenttype_masterpass']."</label></div>" : "";

			$payment_type_choise .= (in_array('PC', $paymentTypes)) ? 
			"<div class=\"media\"><label class=\"checkbox\"><input type=\"radio\" name=\"paymentType\" value=\"PC\"> ".$L['ykbilling_paymenttype_yandex']."</label></div>" : "";

			$payment_type_choise .= "</div>";
		}
		
		$yandex_form = "<form action=\"".$action_url."\" method=\"post\"> 

					<input name=\"shopId\" value=\"".$cfg['plugin']['ykbilling']['shopId']."\" type=\"hidden\"/> 
					<input name=\"scid\" value=\"".$cfg['plugin']['ykbilling']['scid']."\" type=\"hidden\"/> 
					<input name=\"sum\" value=\"".$out_summ."\" type=\"hidden\"> 
					<input name=\"customerNumber\" value=\"".$usr['name']."\" type=\"hidden\"/> 
					".$payment_type_choise."
					<input name=\"orderNumber\" value=\"".$inv_id."\" type=\"hidden\"/> 
					<div class=\"text-center\">
						<input type=\"submit\" value=\"".$L['ykbilling_formbuy']."\" class=\"btn btn-success btn-large\"/> 
					</div>
				</form>";

		$t->assign(array(
			'BILLING_FORM' => $yandex_form,
		));
		$t->parse("MAIN.BILLINGFORM");
		
		cot_payments_updatestatus($pid, 'process'); // Изменяем статус "в процессе оплаты"
	}
	else
	{
		cot_die();
	}
}
elseif ($m == 'success')
{
	$t->assign(array(
		"BILLING_TITLE" => $L['ykbilling_error_title'],
		"BILLING_ERROR" => $L['ykbilling_error_paid']
	));
	
	$t->parse("MAIN.ERROR");
}
elseif ($m == 'fail')
{
	$t->assign(array(
		"BILLING_TITLE" => $L['ykbilling_error_title'],
		"BILLING_ERROR" => $L['ykbilling_error_fail']
	));
	$t->parse("MAIN.ERROR");
}
?>