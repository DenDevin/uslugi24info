<?php

/**
 * [BEGIN_COT_EXT]
 * Hooks=payments.billing.register
 * [END_COT_EXT]
 */
/**
 * Yandex kassa billing Plugin
 *
 * @package ykbilling
 * @version 1.0
 * @author devkont (Yusupov)
 * @copyright (c) CMSWorks Team 2015
 * @license BSD
 */
defined('COT_CODE') or die('Wrong URL.');

require_once cot_incfile('ykbilling', 'plug');

$cot_billings['yandexkassa'] = array(
	'plug' => 'ykbilling',
	'title' => $L['ykbilling_title'],
	'icon' => $cfg['plugins_dir'] . '/ykbilling/images/yk.png'
);