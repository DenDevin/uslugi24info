<!-- BEGIN: MAIN -->

	<!-- BEGIN: BILLINGFORM -->
		<!-- IF !{PHP.cfg.plugin.ykbilling.typechoise} -->
		<div class="alert alert-info">{PHP.L.ykbilling_formtext}</div>
		<script>
			$(document).ready(function(){
				setTimeout(function() {
					$("#yandexform").submit();
				}, 3000);
			});
		</script>
		<!-- ELSE -->
		<h4 class="text-center">{PHP.L.ykbilling_formtext1}</h4>
		<br/>
		<!-- ENDIF -->
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		{BILLING_FORM}
		<br/>
		<br/>
		<br/>
	</div>
</div>
	<!-- END: BILLINGFORM -->

	<!-- BEGIN: ERROR -->
		<h4>{BILLING_TITLE}</h4>
		{BILLING_ERROR}

		<!-- IF {BILLING_REDIRECT_URL} -->
		<br/>
		<p class="small">{BILLING_REDIRECT_TEXT}</p>
		<script>
			$(function(){
				setTimeout(function () {
					location.href='{BILLING_REDIRECT_URL}';
				}, 5000);
			});
		</script>
		<!-- ENDIF -->
	</div>
</div>
	<!-- END: ERROR -->

	


<!-- END: MAIN -->