<?php

/* ====================
 * [BEGIN_COT_EXT]
 * Code=ykbilling
 * Name=Yandex.Kassa
 * Category=Payments
 * Description=Yandex kassa billing system
 * Version=1.2.0
 * Date=
 * Author=Bulat Yusupov (devkont)
 * Copyright=&copy; CMSWorks Team 2015
 * Notes=
 * Auth_guests=RW
 * Lock_guests=12345A
 * Auth_members=RW
 * Lock_members=12345A
 * Requires_modules=payments
 * [END_COT_EXT]
 *
 * [BEGIN_COT_EXT_CONFIG]
 * shopId=01:string:::shopId
 * scid=02:string:::scid
 * shopPassword=03:string:::shopPassword
 * rate=04:string::1:Соотношение суммы к валюте сайта
 * typechoise=05:radio::1:Включить выбор способа оплаты
 * payments=06:string::AC,WM,GP,AB,SB,PB,MA,PC:Способы оплаты (через запятую)
 * testmode=07:radio::0:Тестовый режим
 * [END_COT_EXT_CONFIG]
 */