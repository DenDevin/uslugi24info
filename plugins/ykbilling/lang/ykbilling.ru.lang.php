<?php
/**
 * ykbilling plugin
 *
 * @package ykbilling
 * @version 1.0
 * @author devkont (Yusupov)
 * @copyright Copyright (c) CMSWorks Team 2015
 * @license BSD
 */

defined('COT_CODE') or die('Wrong URL.');

/**
 * Module Config
 */

$L['info_desc'] = 'Платежный плагин Яндекс.Деньги';

$L['cfg_shopId'] = array('shopId', '');
$L['cfg_scid'] = array('scid', '');
$L['cfg_shopPassword'] = array('shopPassword', '');
$L['cfg_rate'] = array('Соотношение суммы к валюте сайта', '');
$L['cfg_typechoise'] = array('Включить выбор способа оплаты', '');

$L['ykbilling_title'] = 'Яндекс.Касса';

$L['ykbilling_paymenttype_yandex'] = '<img src="plugins/ykbilling/images/payments_yamoney.png"/> Яндекс.Деньги';
$L['ykbilling_paymenttype_card'] = '<img src="plugins/ykbilling/images/payments_cards.png"/> Банковские карты';
$L['ykbilling_paymenttype_wmr'] = '<img src="plugins/ykbilling/images/payments_webmoney.png"/> WebMoney (WMR)';
$L['ykbilling_paymenttype_terminal'] = '<img src="plugins/ykbilling/images/payments_terminals.png"/> Терминалы: Связной, Евросеть, Сбербанк';
$L['ykbilling_paymenttype_alfabank'] = '<img src="plugins/ykbilling/images/payments_alfa.png"/>Альфа-Клик';
$L['ykbilling_paymenttype_sberbank'] = '<img src="plugins/ykbilling/images/payments_sber.png"/> Сбербанк';
$L['ykbilling_paymenttype_psb'] = '<img src="plugins/ykbilling/images/payments_psb.png"/> Интернет-банк Промсвязьбанка';
$L['ykbilling_paymenttype_masterpass'] = '<img src="plugins/ykbilling/images/payments_masterpass.png"/> MasterPass';

$L['ykbilling_formtext'] = 'Сейчас вы будете перенаправлены на сайт платежной системы Яндекс.Деньги для проведения оплаты. Если этого не произошло, нажмите кнопку "Перейти к оплате".';
$L['ykbilling_formtext1'] = 'Пожалуйста, выберите наиболее удобный для вас способ оплаты:';
$L['ykbilling_formbuy'] = 'Перейти к оплате';
$L['ykbilling_error_paid'] = 'Оплата прошла успешно. В ближайшее время услуга будет активирована!';
$L['ykbilling_error_done'] = 'Оплата прошла успешно.';
$L['ykbilling_error_incorrect'] = 'Некорректная подпись';
$L['ykbilling_error_otkaz'] = 'Отказ от оплаты.';
$L['ykbilling_error_title'] = 'Результат операции оплаты';
$L['ykbilling_error_fail'] = 'Оплата не произведена! Пожалуйста, повторите попытку. Если ошибка повторится, обратитесь к администратору сайта';

$L['ykbilling_redirect_text'] = 'Сейчас произойдет редирект на страницу оплаченной услуги. Если этого не произошло, перейдите по <a href="%1$s">ссылке</a>.';