<?php
/**
 * ykbilling plugin
 *
 * @package ykbilling
 * @version 1.0
 * @author devkont (Yusupov)
 * @copyright Copyright (c) CMSWorks Team 2015
 * @license BSD
 */

defined('COT_CODE') or die('Wrong URL.');

/**
 * Module Config
 */

$L['info_desc'] = 'Payment plugin Yandex.Money';

$L['cfg_shopId'] = array('shopId', '');
$L['cfg_scid'] = array('scid', '');
$L['cfg_shopPassword'] = array('shopPassword', '');
$L['cfg_rate'] = array('Exchange rate', '');
$L['cfg_typechoise'] = array('Switch payment type choise', '');

$L['ykbilling_title'] = 'Yandex.Kassa';

$L['ykbilling_paymenttype_yandex'] = 'Yandex.Money';
$L['ykbilling_paymenttype_card'] = 'Card';
$L['ykbilling_paymenttype_wmr'] = 'WebMoney (WMR)';
$L['ykbilling_paymenttype_terminal'] = 'Terminal';
$L['ykbilling_paymenttype_alfabank'] = 'Alfa-click';
$L['ykbilling_paymenttype_sberbank'] = 'Sberbank';
$L['ykbilling_paymenttype_psb'] = 'PSB-Retail';
$L['ykbilling_paymenttype_masterpass'] = 'MasterPass';

$L['ykbilling_formtext'] = 'Now you will be redirected to the payment system Yandex.Money for payment. If not, click the "Go to payment".';
$L['ykbilling_formtext1'] = 'Please choose the most convenient way of payment:';
$L['ykbilling_formbuy'] = 'Go to payment';
$L['ykbilling_error_paid'] = 'Payment was successful. In the near future the service will be activated!';
$L['ykbilling_error_done'] = 'Payment was successful.';
$L['ykbilling_error_incorrect'] = 'The signature is incorrect!';
$L['ykbilling_error_otkaz'] = 'Failure to pay.';
$L['ykbilling_error_title'] = 'Result of the operation of payment';
$L['ykbilling_error_fail'] = 'Payment is not made! Please try again. If the problem persists, contact your site administrator';

$L['ykbilling_redirect_text'] = 'Now will redirect to the page of the paid services. If it does not, click <a href="%1$s">here</a>.';