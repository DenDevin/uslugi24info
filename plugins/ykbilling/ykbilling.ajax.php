<?php

/**
 * [BEGIN_COT_EXT]
 * Hooks=ajax
 * [END_COT_EXT]
 */
/**
 * Yandex kassa billing Plugin
 *
 * @package ykbilling
 * @version 1.0
 * @author devkont (Yusupov)
 * @copyright (c) CMSWorks Team 2015
 * @license BSD
 */
defined('COT_CODE') or die('Wrong URL');

require_once cot_incfile('payments', 'module');

header('Content-type: application/xml; charset=UTF-8');

$status_data = $_POST;

if($m == 'checkorder'){
	$hash = mb_strtoupper(md5($status_data['action'].';'.$status_data['orderSumAmount'].';'.$status_data['orderSumCurrencyPaycash'].';'.$status_data['orderSumBankPaycash'].';'.$status_data['shopId'].';'.$status_data['invoiceId'].';'.$status_data['customerNumber'].';'.$cfg['plugin']['ykbilling']['shopPassword']));
	if (empty($status_data)){
		$result_code = 200;
	}elseif($status_data['shopId'] != $cfg['plugin']['ykbilling']['shopId']){
		$result_code = 100;
	}elseif($hash != $status_data['md5']){
		$result_code = 1;
	}else{
		$result_code = 0;
	}
	echo '<?xml version="1.0" encoding="UTF-8"?><checkOrderResponse performedDatetime="'.date(DATE_ATOM).'" code="'.$result_code.'" invoiceId="'.$status_data['invoiceId'].'" shopId="'.$cfg['plugin']['ykbilling']['shopId'].'"/>';
}

if($m == 'paymentaviso'){
	$hash = mb_strtoupper(md5($status_data['action'].';'.$status_data['orderSumAmount'].';'.$status_data['orderSumCurrencyPaycash'].';'.$status_data['orderSumBankPaycash'].';'.$status_data['shopId'].';'.$status_data['invoiceId'].';'.$status_data['customerNumber'].';'.$cfg['plugin']['ykbilling']['shopPassword']));
	if (empty($status_data) || $status_data['shopId'] != $cfg['plugin']['ykbilling']['shopId']){
		$result_code = 200;
	}elseif($hash != $status_data['md5']){
		$result_code = 1;
	}elseif(cot_payments_updatestatus($status_data['orderNumber'], 'paid')){
		$result_code = 0;
	}else{
		$result_code = 200;
	}
	echo '<?xml version="1.0" encoding="UTF-8"?><paymentAvisoResponse performedDatetime="'.date(DATE_ATOM).'" code="'.$result_code.'" invoiceId="'.$status_data['invoiceId'].'" shopId="'.$cfg['plugin']['ykbilling']['shopId'].'"/>';
}