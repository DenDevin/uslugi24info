<?php

/**
 * Yandex kassa billing plugin
 *
 * @package ykbilling
 * @version 1.0
 * @author devkont (Yusupov)
 * @copyright Copyright (c) CMSWorks Team 2015
 * @license BSD
 */

defined('COT_CODE') or die('Wrong URL');

// Requirements
require_once cot_langfile('ykbilling', 'plug');

?>