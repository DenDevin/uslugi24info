<?php


/* ====================
  [BEGIN_COT_EXT]
  Hooks=global
  [END_COT_EXT]
  ==================== */

defined('COT_CODE') or die('Wrong URL.');
if (!defined('COT_ADMIN'))
{
	require_once cot_incfile('primebox', 'plug');
}

require_once cot_incfile('payments', 'module');

if ($primeboxpays = cot_payments_getallpays('primebox', 'paid'))
{
	foreach ($primeboxpays as $pay)
	{	
    $expire = $sys['now'] + $pay['pay_time'];
		if (cot_payments_updatestatus($pay['pay_id'], 'done'))
		{
			$db->update($db_primebox,  array('item_expire' => (int)$expire, 'item_begin' => $sys['now']), "item_id=".(int)$pay['pay_code']);

			/* === Hook === */
			foreach (cot_getextplugins('payprimebox.done') as $pl)
			{
				include $pl;
			}
			/* ===== */
		}
	}
}