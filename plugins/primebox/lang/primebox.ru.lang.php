<?php
/**
 * Plugin Title & Subtitle
 */

$L['info_desc'] = 'Создание рекламных блоков для размещения на сайте. Платное размешение рекламы для пользователей. Счетчики показов и кликов.';

$L['primebox_banner_edit'] = "Редактировать баннер";
$L['primebox_banner_new'] = "Создание баннера";
$L['primebox_banners'] = "primebox";
$L['primebox_category_no'] = "Для создания баннера нужно создать хотябы одну категорию";

$L['primebox_alt'] = "Альтернативный текст картинки";
$L['primebox_click_url'] = "Url для перехода";
$L['primebox_clicks'] = "Клики";
$L['primebox_client'] = "Клиент";
$L['primebox_extrainfo'] = "Дополнительная информация";
$L['primebox_showcount'] = "Общее число показов";
$L['primebox_expire'] = "Завершение публикации";
$L['primebox_saved'] = "Сохранено";
$L['primebox_publish'] = "Разместить";

$L['primebox_payments_desc'] = "Оплата за размещение объявления";

$L['primebox_expire_time'] = "Период резмещения";
$L['primebox_place'] = "Место для резмещения";

$L['primebox_paymentinterval_day'] = 'день';
$L['primebox_paymentinterval_week'] = 'неделю';
$L['primebox_paymentinterval_month'] = 'месяц';

$Ls['primebox_day'] = "день,дня,дней";
$Ls['primebox_week'] = "неделя,недели,недель";
$Ls['primebox_month'] = "месяц,месяца,месяцев";

$L['primebox_paymentinterval'] = $L['primebox_paymentinterval_'.$cfg['plugin']['primebox']['purchase_period']];

$L['primebox_err_titleempty'] = "Заголовок не может быть пустым";
$L['primebox_err_inv_file_type'] = "Недопустимый тип файла";

$L['primebox_cfg_price'] = "Цена за период"; 