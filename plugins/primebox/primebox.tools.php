<?php

/* ====================
  [BEGIN_COT_EXT]
  Hooks=tools
  [END_COT_EXT]
  ==================== */


(defined('COT_CODE') && defined('COT_ADMIN')) or die('Wrong URL.');


list($usr['auth_read'], $usr['auth_write'], $usr['isadmin']) = cot_auth('plug', 'primebox');
cot_block($usr['isadmin']);

require_once(cot_incfile('primebox', 'plug'));
require_once cot_langfile('primebox', 'plug');

if ($n != 'track')
{
	$n = 'main';
}

if(empty($a))
{
	$a = 'index';
}

if (file_exists(cot_incfile('primebox', 'plug', 'admin.'.$n.'.'.$a)))
{
	$t = new XTemplate(cot_tplfile('primebox.admin.'.$n.'.'.$a, 'plug'));
	require_once cot_incfile('primebox', 'plug', 'admin.'.$n.'.'.$a);
	$t->parse('MAIN');
	$adminmain = $t->text('MAIN');
}
else
{
	cot_die_message(404);
	exit;
}