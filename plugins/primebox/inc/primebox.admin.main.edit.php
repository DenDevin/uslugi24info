<?php

(defined('COT_CODE') && defined('COT_ADMIN')) or die('Wrong URL.');

global $adminpath, $structure, $cfg, $L, $usr, $sys;

$adminpath[] = array(cot_url('admin', array('m' => 'other', 'p' => 'primebox')), $L['primebox_primebox']);

if (empty($structure['primebox']))
{
	cot_error($L['primebox_category_no']);
}

$id = cot_import('id', 'G', 'INT');
$maxrowsperpage = $cfg['maxrowsperpage'];
list($pg, $d, $durl) = cot_import_pagenav('d', $maxrowsperpage);
$interval = cot_import('interval', 'G', 'TXT');

$act = cot_import('act', 'P', 'ALP');
if (!$id)
{
	$id = 0;
	$adminpath[] = '&nbsp;'.$L['primebox'];
	$primebox = array();
}
else
{
	$primebox = $db->query("SELECT * FROM $db_primebox WHERE item_id = ".(int)$id." LIMIT 1")->fetch();
	$adminpath[] = $L['primebox_banner_edit'].": ".htmlspecialchars($primebox['primebox_title']);
}

if ($act == 'save')
{
	$item = array();

	$item['item_title'] = cot_import('rtitle', 'P', 'TXT');
	if (empty($item['item_title']))
	{
		cot_error($L['primebox_err_titleempty'], 'rtitle');
	}
	$item['item_cat'] = cot_import('rcat', 'P', 'TXT');
	$file = primebox_import_file('rfile', $primebox['primebox_file']);
	$delFile = cot_import('rdel_rfile', 'P', 'BOL') ? 1 : 0;
	if ($delFile)
	{
		$item['item_file'] = '';
	}

	if (!empty($file))
	{
		@$gd = getimagesize($file);
		if (!$gd)
		{
			cot_error($L['primebox_err_inv_file_type'], 'rfile');
		}
		else
		{
			switch ($gd[2])
			{
				case IMAGETYPE_GIF:
				case IMAGETYPE_JPEG:
				case IMAGETYPE_PNG:
				case IMAGETYPE_BMP:
						$item['item_filetype'] = 'img';
					break;
				case IMAGETYPE_SWF:
				case IMAGETYPE_SWC:
						$item['item_filetype'] = 'swf';
					break;
				default:
					cot_error($L['primebox_err_inv_file_type'], 'rfile');
			}
		}
	}
	else
  {
		if (!$delFile)
		{
			unset($item['item_filetype']);
		}
	}

	$item['item_alt'] = cot_import('ralt', 'P', 'TXT');
	$item['item_clickurl'] = cot_import('rclickurl', 'P', 'TXT');
	$item['item_description'] = cot_import('rdescription', 'P', 'TXT');
	$item['item_expire'] = cot_import_date('rexpire');
	$item['item_userid'] = cot_import('ruserid', 'P', 'INT');

	$item['item_begin'] = $sys['now'];

	// Extra fields
	foreach ($cot_extrafields[$db_primebox] as $exfld)
	{
		$item['item_' . $exfld['field_name']] = cot_import_extrafields('r' . $exfld['field_name'], $exfld);
	}
	
	if (!cot_error_found())
	{
		if (!empty($file))
		{
			$item['item_file'] = $file;
		}
		if ($id > 0)
		{
			$db->update($db_primebox, $item, "item_id = ".(int)$id);
			cot_log("Edited adv # {$id} - {$data['item_title']}", 'adm');
		}
		else
		{
			$db->insert($db_primebox, $item);
			$id = $db->lastInsertId();
			cot_log("Added new adv # {$id} - {$item['item_title']}", 'adm');			
		}
		if (!empty($primebox['item_file']) && isset($data['item_file']) && $primebox['item_file'] != $data['item_file'] && file_exists($asd['item_file']))
		{
			unlink($ad['item_file']);
		}
		cot_extrafield_movefiles();
		cot_message($L['item_saved']);
		
		cot_redirect(cot_url('admin', array('m' => 'other', 'p' => 'primebox'), '', true));
	}
	else
	{
		if (!empty($file) && file_exists($file))
			unlink($file);
	}
}

$delUrl = ($ad['item_id'] > 0) ? cot_confirm_url(cot_url('admin', 'm=other&p=primebox&act=delete&id='.$primebox['item_id'].'&'.cot_xg()), 'admin') : '';


$where = 'WHERE user_id='.(($primebox['item_userid'] > 0) ? $primebox['item_userid'] : $usr['id']);

$sql = $db->query("SELECT user_id, user_name FROM $db_users $where ORDER BY `user_id` ASC");
$clients = $sql->fetchAll(PDO::FETCH_KEY_PAIR);
$clients = (!$clients) ? array() : $clients;

$formFile = cot_inputbox('file', 'rfile', $primebox['item_file']);
if (!empty($primebox['item_file']))
	$formFile .= cot_checkbox(false, 'rdel_rfile', $L['Delete']);

$t->assign(array(
	'FORM_ID' => $primebox['item_id'],
	'FORM_TITLE' => cot_inputbox('text', 'rtitle', $primebox['item_title']),
	'FORM_CATEGORY' => cot_selectbox_structure('primebox', $primebox['item_cat'], 'rcat', '', false, false),
	'FORM_FILE' => $formFile,
	'FORM_IMAGE' => $primebox['item_file'],
	'FORM_ALT' => cot_inputbox('text', 'ralt', $primebox['item_alt']),
	'FORM_CLICKURL' => cot_inputbox('text', 'rclickurl', $primebox['item_clickurl']),
	'FORM_DESCRIPTION' => cot_textarea('rdescription', $primebox['item_description'], 5, 60),
	'FORM_CLIENT_ID' => cot_selectbox($primebox['item_userid'], 'ruserid', array_keys($clients), array_values($clients), false),
	'FORM_PUBLISH_DOWN' => cot_selectbox_date($primebox['item_expire'], 'long', 'rexpire'),
	'FORM_DELETE_URL' => $delUrl,
));

foreach ($cot_extrafields[$db_primebox] as $exfld)
{
	$uname = strtoupper($exfld['field_name']);
	$exfld_val = cot_build_extrafields('r' . $exfld['field_name'], $exfld, $primebox['item_'.$exfld['field_name']]);
	$exfld_title =  isset($L['item_' . $exfld['field_name'] . '_title']) ? $L['item_' . $exfld['field_name'] . '_title'] : $exfld['field_description'];
	$t->assign(array(
		'FORM_' . $uname => $exfld_val,
		'FORM_' . $uname . '_TITLE' => $exfld_title,
		'FORM_EXTRAFLD' => $exfld_val,
		'FORM_EXTRAFLD_TITLE' => $exfld_title
		));
	$t->parse('MAIN.EXTRAFLD');
}

cot_display_messages($t);
$t->assign(array(
	'PAGE_TITLE' => isset($primebox['item_id']) ? $L['primebox_banner_edit'].": ".htmlspecialchars($primebox['item_title']) :
		$L['primebox_banner_new'],
));