<?php

(defined('COT_CODE') && defined('COT_PLUG')) or die('Wrong URL.');

global $structure, $cfg, $L, $usr, $sys;

$id = cot_import('id', 'G', 'INT');

$maxrowsperpage = $cfg['maxrowsperpage'];
list($pg, $d, $durl) = cot_import_pagenav('d', $maxrowsperpage);

$act = cot_import('act', 'G', 'ALP');

if (!$id)
{
	$id = 0;
	$primebox = array();
}
else
{
	$primebox = $db->query("SELECT * FROM $db_primebox WHERE item_id = ".(int)$id." AND item_userid=".$usr['id']." LIMIT 1")->fetch();
  if(!$primebox)
  {
    cot_die();
  }
}

if ($act == 'add')
{
	$item = array();

	$item['item_title'] = cot_import('rtitle', 'P', 'TXT');
	if (empty($item['item_title']))
	{
		cot_error($L['primebox_err_titleempty'], 'rtitle');
	}
	$item['item_cat'] = cot_import('rcat', 'P', 'TXT');
	$file = primebox_import_file('rfile', $primebox['primebox_file']);
	$delFile = cot_import('rdel_rfile', 'P', 'BOL') ? 1 : 0;
	if ($delFile)
	{
		$item['item_file'] = '';
	}
  
	if (!empty($file))
	{
		@$gd = getimagesize($file);
		if (!$gd)
		{
			cot_error($L['primebox_err_inv_file_type'], 'rfile');
		}
		else
		{
			switch ($gd[2])
			{
				case IMAGETYPE_GIF:
				case IMAGETYPE_JPEG:
				case IMAGETYPE_PNG:
				case IMAGETYPE_BMP:
						$item['item_filetype'] = 'img';
					break;
				case IMAGETYPE_SWF:
				case IMAGETYPE_SWC:
						$item['item_filetype'] = 'swf';
					break;
				default:
					cot_error($L['primebox_err_inv_file_type'], 'rfile');
			}
		}
	}
	else
  {
		if (!$delFile)
		{
			unset($item['item_filetype']);
		}
	}

  $period = cot_import('rperiod', 'P', 'INT');
  $item['item_period'] = $period;
	$item['item_alt'] = cot_import('ralt', 'P', 'TXT');
	$item['item_clickurl'] = cot_import('rclickurl', 'P', 'TXT');
	$item['item_description'] = cot_import('rdescription', 'P', 'TXT');
	$item['item_userid'] = $usr['id'];
  
	// Extra fields
	foreach ($cot_extrafields[$db_primebox] as $exfld)
	{
		$item['item_' . $exfld['field_name']] = cot_import_extrafields('r' . $exfld['field_name'], $exfld);
	}
	
	if (!cot_error_found())
	{
		if (!empty($file))
		{
			$item['item_file'] = $file;
		}
    
			$db->insert($db_primebox, $item);
			$id = $db->lastInsertId();
      
      require_once cot_incfile('configuration');
      $structure['primebox'] = (is_array($structure['primebox'])) ? $structure['primebox'] : array();
      $configlist = cot_config_list('plug', 'primebox', $item['item_cat']);
      
      if($cfg['plugin']['primebox']['purchase_period'] == 'day')
       {	
			   $options['time'] = $period * 24 * 60 * 60;
			 }
       elseif($cfg['plugin']['primebox']['purchase_period'] == 'week')
       {
			   $options['time'] = $period * 7 * 24 * 60 * 60;     
       }
       else
       {
			   $options['time'] = $period * 30 * 24 * 60 * 60;       
       }
       
   	  $summ = $period * $configlist['price']['config_value'];  
      $options['desc'] = $L['primebox_payments_desc'];
			$options['code'] = $id;
		
			if ($db->fieldExists($db_payments, "pay_redirect")){
				$options['redirect'] = $cfg['mainurl'].'/'.cot_url('plug', 'e=primebox', '', true);
			}
    
		if (!empty($primebox['item_file']) && isset($data['item_file']) && $primebox['item_file'] != $data['item_file'] && file_exists($asd['item_file']))
		{
			unlink($ad['item_file']);
		}
		cot_extrafield_movefiles();
	//	cot_message($L['item_saved']);
		cot_primebox_sync($item['item_cat']);
    
    cot_payments_create_order('primebox', $summ, $options);
          
		cot_redirect(cot_url('plug', array('e' => 'primebox'), '', true));
	}
	else
	{
		if (!empty($file) && file_exists($file))
			unlink($file);
	}
}

if ($act == 'save' && $id > 0)
{
	$item = array();
  
	$item['item_title'] = cot_import('rtitle', 'P', 'TXT');
	if (empty($item['item_title']))
	{
		cot_error($L['primebox_err_titleempty'], 'rtitle');
	}

	$file = primebox_import_file('rfile', $primebox['primebox_file']);
	$delFile = cot_import('rdel_rfile', 'P', 'BOL') ? 1 : 0;
	if ($delFile)
	{
		$item['item_file'] = '';
	}

	if (!empty($file))
	{
		@$gd = getimagesize($file);
		if (!$gd)
		{
			cot_error($L['primebox_err_inv_file_type'], 'rfile');
		}
		else
		{
			switch ($gd[2])
			{
				case IMAGETYPE_GIF:
				case IMAGETYPE_JPEG:
				case IMAGETYPE_PNG:
				case IMAGETYPE_BMP:
						$item['item_filetype'] = 'img';
					break;
				case IMAGETYPE_SWF:
				case IMAGETYPE_SWC:
						$item['item_filetype'] = 'swf';
					break;
				default:
					cot_error($L['primebox_err_inv_file_type'], 'rfile');
			}
		}
	}
	else
  {
		if (!$delFile)
		{
			unset($item['item_filetype']);
		}
	}
  
  if($primebox['item_expire'] == 0 && $primebox['item_paused'] == 0)
  {
   $item['item_cat'] = cot_import('rcat', 'P', 'TXT');
   $item['item_period'] = cot_import('rperiod', 'P', 'INT');
	}
  
  $item['item_alt'] = cot_import('ralt', 'P', 'TXT');
	$item['item_clickurl'] = cot_import('rclickurl', 'P', 'TXT');
	$item['item_description'] = cot_import('rdescription', 'P', 'TXT');

	foreach ($cot_extrafields[$db_primebox] as $exfld)
	{
		$item['item_' . $exfld['field_name']] = cot_import_extrafields('r' . $exfld['field_name'], $exfld);
	}
	
	if (!cot_error_found())
	{
		if (!empty($file))
		{
			$item['item_file'] = $file;
		}
		
    $db->update($db_primebox, $item, "item_id = ".(int)$id);
    
		if (!empty($primebox['item_file']) && isset($data['item_file']) && $primebox['item_file'] != $data['item_file'] && file_exists($asd['item_file']))
		{
			unlink($ad['item_file']);
		}
    
		cot_extrafield_movefiles();
		cot_message($L['item_saved']);
    
    if($primebox['item_expire'] == 0 && $primebox['item_paused'] == 0)
    {
		  cot_primebox_sync($item['item_cat']);
    }
    
		cot_redirect(cot_url('plug', array('e' => 'primebox'), '', true));
	}
	else
	{
		if (!empty($file) && file_exists($file))
			unlink($file);
	}
}
elseif ($act == 'buy' && $primebox['item_expire'] == 0 && $primebox['item_paused'] == 0)
{
   require_once cot_incfile('configuration');
   $structure['primebox'] = (is_array($structure['primebox'])) ? $structure['primebox'] : array();
   $configlist = cot_config_list('plug', 'primebox', $primebox['item_cat']);
   
   $period = $primebox['item_period'];
       
   if($cfg['plugin']['primebox']['purchase_period'] == 'day')
    {	
			$options['time'] = $period * 24 * 60 * 60;
		}
    elseif($cfg['plugin']['primebox']['purchase_period'] == 'week')
    {
			$options['time'] = $period * 7 * 24 * 60 * 60;     
    }
    else
    {
			$options['time'] = $period * 30 * 24 * 60 * 60;       
    }
       
   $summ = $period * $configlist['price']['config_value'];  
   $options['desc'] = $L['primebox_payments_desc'];
	 $options['code'] = $id;
		
	 if ($db->fieldExists($db_payments, "pay_redirect")){
			$options['redirect'] = $cfg['mainurl'].'/'.cot_url('plug', 'e=primebox', '', true);
	 }
      
   cot_payments_create_order('primebox', $summ, $options);
   
   cot_redirect(cot_url('plug', array('e' => 'primebox'), '', true));
}
elseif ($act == 'paused' && $primebox['item_expire'] > $sys['now'] && $primebox['item_paused'] == 0 && $id > 0)
{
   $db->update($db_primebox,  array('item_expire' => 0, 'item_paused' => ($primebox['item_expire'] - $sys['now'])), "item_id=".(int)$id);
   
   cot_redirect(cot_url('plug', array('e' => 'primebox'), '', true));
}
elseif ($act == 'unpaused' && $primebox['item_paused'] > 0 && $id > 0)
{
   $db->update($db_primebox,  array('item_expire' => ($primebox['item_paused'] + $sys['now']), 'item_paused' => 0), "item_id=".(int)$id);
   
   cot_redirect(cot_url('plug', array('e' => 'primebox'), '', true));
}
elseif ($act == 'del' && $primebox['item_expire'] < $sys['now'] && $primebox['item_paused'] == 0 && $id > 0)
{
	$urlArr = $list_url_path;
	if ($pagenav['current'] > 0)
		$urlArr['d'] = $pagenav['current'];
	$id = cot_import('id', 'G', 'INT');
	
	$item = $db->query("SELECT * FROM $db_primebox WHERE item_id = ".(int)$id." AND item_userid=".$usr['id']." LIMIT 1")->fetch();
	if (!$item)
	{
		cot_die();
	}
	
	$db->delete($db_primebox, "item_id = ".(int)$id);
	if (file_exists($item['item_file']))
		unlink($item['item_file']);

	foreach ($cot_extrafields[$db_primebox] as $exfld)
	{
		cot_extrafield_unlinkfiles($item['item_' . $exfld['field_name']], $exfld);
	}

	cot_redirect(cot_url('plug', 'e=primebox', '', true));
}

$t = new XTemplate(cot_tplfile(array('primebox', 'main'), 'plug'));
cot_display_messages($t);

$where = 'WHERE user_id='.(($primebox['item_userid'] > 0) ? $primebox['item_userid'] : $usr['id']);

$formFile = cot_inputbox('file', 'rfile', $primebox['item_file']);
if (!empty($primebox['item_file']))
	$formFile .= cot_checkbox(false, 'rdel_rfile', $L['Delete']);

$period = ($cfg['plugin']['primebox']['purchase_period'] == 'day') ? range(1, 31) : range(1, 12);
$t->assign(array(
	'PRIMEBOX_FORM_ID' => $primebox['item_id'],
	'PRIMEBOX_FORM_TITLE' => cot_inputbox('text', 'rtitle', $primebox['item_title'], array('size' => '20', 'maxlength' => '32')),
	'PRIMEBOX_FORM_CATEGORY' => primebox_selectbox($primebox['item_cat'], 'rcat', false),
	'PRIMEBOX_FORM_FILE' => $formFile,
	'PRIMEBOX_FORM_IMAGE' => $primebox['item_file'],
	'PRIMEBOX_FORM_PERIOD' => cot_selectbox($primebox['item_period'], 'rperiod', $period, $period, false),
	'PRIMEBOX_FORM_ALT' => cot_inputbox('text', 'ralt', $primebox['item_alt']),
	'PRIMEBOX_FORM_CLICKURL' => cot_inputbox('text', 'rclickurl', $primebox['item_clickurl']),
	'PRIMEBOX_FORM_DESCRIPTION' => cot_textarea('rdescription', $primebox['item_description'], 5, 60),
));

foreach ($cot_extrafields[$db_primebox] as $exfld)
{
	$uname = strtoupper($exfld['field_name']);
	$exfld_val = cot_build_extrafields('r' . $exfld['field_name'], $exfld, $primebox['item_'.$exfld['field_name']]);
	$exfld_title =  isset($L['item_' . $exfld['field_name'] . '_title']) ? $L['item_' . $exfld['field_name'] . '_title'] : $exfld['field_description'];
	$t->assign(array(
		'PRIMEBOX_FORM_' . $uname => $exfld_val,
		'PRIMEBOX_FORM_' . $uname . '_TITLE' => $exfld_title,
		'PRIMEBOX_FORM_EXTRAFLD' => $exfld_val,
		'PRIMEBOX_FORM_EXTRAFLD_TITLE' => $exfld_title
		));
	$t->parse('MAIN.EDIT.EXTRAFLD');
	$t->parse('MAIN.SHOW.EXTRAFLD');
}

$t->assign(array(
	'PAGE_TITLE' => isset($primebox['item_id']) ? $L['primebox_banner_edit'].": ".htmlspecialchars($primebox['item_title']) :
		$L['primebox_banner_new'],
));


$list = $db->query("SELECT * FROM $db_primebox WHERE item_userid=".$usr['id']."")->fetchAll();

$jj=0;
foreach ($list as $item)
{
  $jj++;
	$t->assign(primebox_generate_tags($item, 'PRIMEBOX_ROW_'));
	$t->assign(array(
		'PRIMEBOX_ROW_NUM' => $jj,
	));
	$t->parse('MAIN.SHOW.PRIMEBOX_ROW');
}

$t->assign(array(
	'PRIMEBOX_COUNT' => $jj
));

if($id > 0)
{
 $t->parse('MAIN.EDIT');
}
else
{
 $t->parse('MAIN.SHOW');
}