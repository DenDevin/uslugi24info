<?php

defined('COT_CODE') or die('Wrong URL.');

global $db, $db_primebox, $sys, $cfg;

$id = cot_import('id', 'G', 'INT');
$primebox = $db->query("SELECT * FROM $db_primebox WHERE item_id = ".(int)$id." LIMIT 1")->fetch();
if (empty($primebox))
{
	cot_die_message(404, TRUE);
}
elseif (!empty($primebox['item_clickurl']))
{
  primebox_tracks($id, 'click');
	header('Location: '.$primebox['item_clickurl']);
}
exit();
