<?php

defined('COT_CODE') or die('Wrong URL.');
require_once cot_incfile('extrafields');

// Tables and extras
cot::$db->registerTable('primebox');
cot_extrafields_register_table('primebox');

$primebox_allowed_ext = array('bmp', 'gif', 'jpg', 'jpeg', 'swf', 'png');
$primebox_files_dir = 'datas/primebox/';

function primebox_show($cat = '', $cnt = 1, $tpl = '') {
    global $sys, $usr, $cfg, $db, $db_users, $db_primebox;

    if (empty($tpl))
        $tpl = $cat;

    $cats = array();
    $where = array();
    $cnt = (int) $cnt;

    $where['item_published'] = "item_paused=0";
    $where['item_expire'] = "item_expire >='" . (int) $sys['now'] . "'";

    $cat = str_replace(' ', '', $cat);

    if ($cat != '') {
        $categs = explode(',', $cat);
        foreach ($categs as $tmp) {
            $tmp = trim($tmp);
            if (empty($tmp)) {
                continue;
            }
            if ($subcats) {
                $cats = array_merge($cats, cot_structure_children('primebox', $tmp, true, true, false, false));
            } else {
                $cats[] = $tmp;
            }
        }
        $cats = array_unique($cats);
    }
    

    if (count($cats) > 0) {
        $where['item_cat'] = 'item_cat IN ("' . implode('", "', $cats) . '")';
    }

    $where = (!empty($where)) ? ' WHERE ' . implode(' AND ', $where) : '';

    $primebox = $db->query("SELECT * FROM $db_primebox 
         $where ORDER BY item_lastshow ASC LIMIT $cnt")->fetchAll();

    $t = new XTemplate(cot_tplfile(array('primebox', $tpl), 'plug'));
    $jj = 0;
    foreach ($primebox as $ad) {
        primebox_tracks($ad['item_id'], 'show');  // записываем показ
        $jj++;
        $t->assign(primebox_generate_tags($ad, 'PRIMEBOX_ROW_'));
        $t->assign(array(
            'PRIMEBOX_ROW_NUM' => $jj,
        ));
        $t->parse('MAIN.ROW');
    }
    $t->parse('MAIN');

    return $t->text('MAIN');
}

function primebox_import_file($inputname, $oldvalue = '') {
    global $lang, $L, $cot_translit, $primebox_allowed_ext, $primebox_files_dir, $cfg;

    $import = !empty($_FILES[$inputname]) ? $_FILES[$inputname] : array();
    $import['delete'] = cot_import('rdel_' . $inputname, 'P', 'BOL') ? 1 : 0;

    if (is_array($import) && !$import['error'] && !empty($import['name'])) {
        $fname = mb_substr($import['name'], 0, mb_strrpos($import['name'], '.'));
        $ext = mb_strtolower(mb_substr($import['name'], mb_strrpos($import['name'], '.') + 1));

        if (!file_exists($primebox_files_dir)) {
            mkdir($primebox_files_dir);
        }
        if (empty($primebox_allowed_ext) || in_array($ext, $primebox_allowed_ext)) {
            if ($lang != 'en') {
                require_once cot_langfile('translit', 'core');
                $fname = (is_array($cot_translit)) ? strtr($fname, $cot_translit) : '';
            }
            $fname = str_replace(' ', '_', $fname);
            $fname = preg_replace('#[^a-zA-Z0-9\-_\.\ \+]#', '', $fname);
            $fname = str_replace('..', '.', $fname);
            $fname = (empty($fname)) ? cot_unique() : $fname;

            $fname .= (file_exists("{$primebox_files_dir}/$fname.$ext") && $oldvalue != $fname . '.' . $ext) ? date("YmjGis") : '';
            $fname .= '.' . $ext;

            $file['old'] = (!empty($oldvalue) && ($import['delete'] || $import['tmp_name'])) ? $oldvalue : '';
            $file['tmp'] = (!$import['delete']) ? $import['tmp_name'] : '';
            $file['new'] = (!$import['delete']) ? $primebox_files_dir . $fname : '';

            if (!empty($file['old']) && file_exists($file['old'])) {
                unlink($file['old']);
            }
            if (!empty($file['tmp']) && !empty($file['tmp'])) {
                move_uploaded_file($file['tmp'], $file['new']);
            }
            return $file['new'];
        } else {
            cot_error($L['primebox_err_inv_file_type'], $inputname);
            return '';
        }
    }
}

function primebox_generate_tags($primebox, $tagPrefix = '') {
    global $cfg, $L, $Ls, $sys, $usr, $structure, $cot_extrafields, $db_primebox;

    $temp_array = array();

    if (is_int($primebox) && $primebox > 0) {
        $primebox = $db->query("SELECT * FROM $db_primebox WHERE item_id = " . (int) $primebox . " LIMIT 1")->fetch();
    }
    if ($primebox['item_id'] > 0) {
        $temp_array = array(
            'EDIT_URL' => cot_url('plug', array('e' => 'primebox', 'a' => 'edit', 'id' => $primebox['item_id'])),
            'DEL_URL' => ($primebox['item_expire'] < $sys['now'] && $primebox['item_paused'] == 0) ? cot_url('plug', array('e' => 'primebox', 'a' => 'edit', 'act' => 'del', 'id' => $primebox['item_id'])) : '',
            'URL' => $primebox['item_clickurl'],
            'ALT' => $primebox['item_alt'],
            'ID' => $primebox['item_id'],
            'TITLE' => htmlspecialchars($primebox['item_title']),
            'DESCRIPTION' => $primebox['item_description'],
            'CLICKS' => $primebox['item_track_clicks'],
            'SHOWS' => $primebox['item_track_shows'],
            'CATEGORY' => $primebox['item_cat'],
            'CATEGORY_TITLE' => htmlspecialchars($structure['primebox'][$primebox['item_cat']]['title']),
            'CLICKS_PERSENT' => ($primebox['item_track_shows'] > 0) ?
                    round($primebox['item_track_clicks'] / $primebox['item_track_shows'] * 100, 0) . " %" : '0 %',
            'FILETYPE' => $primebox['item_filetype'],
            'IMAGE' => $primebox['item_file'],
            'FILETYPE' => $primebox['item_filetype'],
            'CLICK_URL' => cot_url('plug', 'e=primebox&a=click&id=' . $primebox['item_id']),
            'EXPIRE' => $primebox['item_expire'],
            'PAUSED' => $primebox['item_paused'],
            'PAUSED_TIME' => ($primebox['item_paused'] > 0) ? cot_build_timegap($sys['now'] - $primebox['item_paused']) : '',
            'PERIOD' => cot_declension($primebox['item_period'], $Ls['primebox_' . $cfg['plugin']['primebox']['purchase_period']]),
        );

        foreach ($cot_extrafields[$db_primebox] as $exfld) {
            $tag = mb_strtoupper($exfld['field_name']);
            $exfld_val = cot_build_extrafields_data('item_', $exfld, $row['item_' . $exfld['field_name']]);
            $exfld_title = isset($L['item_' . $exfld['field_name'] . '_title']) ? $L['item_' . $exfld['field_name'] . '_title'] : $exfld['field_description'];
            $temp_array[$tag . '_TITLE'] = $exfld_title;
            $temp_array[$tag] = $exfld_val;
            $temp_array[$tag . '_VALUE'] = $row['item_' . $exfld['field_name']];
        }
    }

    $return_array = array();
    foreach ($temp_array as $key => $val) {
        $return_array[$tagPrefix . $key] = $val;
    }

    return $return_array;
}

function cot_primebox_sync($cat) {
    global $db, $db_structure, $db_primebox;
    $sql = $db->query("SELECT COUNT(*) FROM $db_primebox
        WHERE item_cat='" . $db->prep($cat) . "'");
    return (int) $sql->fetchColumn();
}

function cot_primebox_updatecat($oldcat, $newcat) {
    global $db, $db_primebox;
    return (bool) $db->update($db_primebox, array("item_cat" => $newcat), "item_cat='" . $db->prep($oldcat) . "'");
}

function primebox_selectbox($check, $name, $add_empty = false) {
    global $structure, $cfg, $L;
    require_once cot_incfile('configuration');
    $structure['primebox'] = (is_array($structure['primebox'])) ? $structure['primebox'] : array();
    $result_array = array();
    foreach ($structure['primebox'] as $i => $x) {
        $configlist = cot_config_list('plug', 'primebox', $i);
        if ($i != 'all') {
            $result_array[$i] = $x['tpath'] . ' (' . $configlist['price']['config_value'] . ' ' . $cfg['payments']['valuta'] . ' в ' . $L['primebox_paymentinterval'] . ')';
        }
    }
    $result = cot_selectbox($check, $name, array_keys($result_array), array_values($result_array), $add_empty);

    return($result);
}

function primebox_tracks($id, $type = 'show') {
    global $db, $sys, $cfg, $db_primebox;

    if (cot_import('e', 'G', 'TXT') != 'primebox') {
        if ($type == 'show') {
            $db->query("UPDATE $db_primebox SET item_track_shows = item_track_shows+1, item_lastshow = " . $sys['now'] . " WHERE item_id = " . (int) $id . "");
        } elseif ($type == 'click') {
            $db->query("UPDATE $db_primebox SET item_track_clicks = item_track_clicks+1 WHERE item_id = " . (int) $id . "");
        }
    }
}
