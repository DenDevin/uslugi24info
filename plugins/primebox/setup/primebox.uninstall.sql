/**
 * Completely removes primebox data and tables
 */

DROP TABLE IF EXISTS `cot_primebox`;

DELETE FROM `cot_structure` WHERE `structure_area` = 'primebox';