<!-- BEGIN: MAIN -->
<div>
    <a href="{PHP|cot_url('admin', 'm=other&p=primebox')}">Объявления</a>
    <a href="{PHP|cot_url('admin', 'm=structure&n=primebox')}">Категории</a>
    <a href="{PHP.db_primebox|cot_url('admin', 'm=extrafields&n=$this')}">Экстраполя</a>
</div>

{FILE "{PHP.cfg.themes_dir}/{PHP.cfg.defaulttheme}/warnings.tpl"}

<div class="block">
	<h3 class="tags">{PAGE_TITLE}</h3>
	<form action="{FORM_ID|cot_url('admin', 'm=other&p=primebox&a=edit&id=$this')}" method="POST" ENCTYPE="multipart/form-data">
	   <!-- <input type="hidden" name="rid" value="{FORM_ID}" /> -->
		<input type="hidden" name="act" value="save" />

		<table class="cells">
			<tr>
				<td class="width20">{PHP.L.Title}:</td>
				<td>{FORM_TITLE}</td>
			</tr>
			<tr>
				<td>{PHP.L.Category}:</td>
				<td>{FORM_CATEGORY}</td>
			</tr>
			<tr>
				<td>{PHP.L.Image}:</td>
				<td>
					<div class="marginbottom10">{FORM_IMAGE}</div>
					{FORM_FILE}
				</td>
			</tr>
			<tr>
				<td>{PHP.L.primebox_alt}:</td>
				<td>{FORM_ALT}</td>
			</tr>
			<tr>
				<td>{PHP.L.primebox_click_url}:</td>
				<td>
         {FORM_CLICKURL}<br>
         <small>В формате http://mysite.ru</small>
        </td>
			</tr>
			<tr>
				<td>{PHP.L.Description}:</td>
				<td>{FORM_DESCRIPTION}</td>
			</tr>
			<tr>
				<td>{PHP.L.primebox_expire}:</td>
				<td>{FORM_PUBLISH_DOWN}</td>
			</tr>
			<!-- BEGIN: EXTRAFLD -->
			<tr>
				<td>{FORM_EXTRAFLD_TITLE}:</td>
				<td>{FORM_EXTRAFLD}</td>
			</tr>
			<!-- END: EXTRAFLD -->
			<tr>
				<td>{PHP.L.primebox_client}:</td>
				<td>{FORM_CLIENT_ID}</td>
			</tr>
		</table>
		<div class="action_bar valid">
			<button type="submit" class="submit">{PHP.L.Submit}</button>
		</div>

	</form>
<!-- END: MAIN -->