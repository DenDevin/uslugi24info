<!-- BEGIN: MAIN -->

<!-- BEGIN: ROW -->
<div class="well">
     <h4><a href="{PRIMEBOX_ROW_CLICK_URL}">{PRIMEBOX_ROW_TITLE}</a></h4>
       <div>
        <a href="{PRIMEBOX_ROW_CLICK_URL}">
         <!-- IF {PRIMEBOX_ROW_IMAGE} -->
         <img src="{PRIMEBOX_ROW_IMAGE}" class="width100" alt="{PRIMEBOX_ROW_ALT}"/>
         <!-- ELSE -->
         <div class="textcenter">
           {PRIMEBOX_ROW_DESCRIPTION}
         </div>
         <!-- ENDIF -->
         </a>
       </div>
  <div class="clear"></div>
</div>
<!-- END: ROW -->

<!-- END: MAIN -->