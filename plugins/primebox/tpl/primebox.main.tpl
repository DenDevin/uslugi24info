<!-- BEGIN: MAIN -->           
<div class="row">
 <div class="span12">    
  <!-- BEGIN: EDIT -->
    <h1>{PAGE_TITLE}</h1>
		{FILE "{PHP.cfg.themes_dir}/{PHP.cfg.defaulttheme}/warnings.tpl"}
      <form action="{PRIMEBOX_FORM_ID|cot_url('plug', 'e=primebox&a=edit&act=save&id='$this)}" method="POST" ENCTYPE="multipart/form-data" class="form-horizontal">
        <table class="table">
					<tbody>
          <tr>
						<td class="width30">{PHP.L.Title}:</td>
						<td class="width70">{PRIMEBOX_FORM_TITLE}</td>
					</tr>
          <!-- IF {PHP.primebox.item_expire} == 0 AND {PHP.primebox.item_paused} == 0 -->
					<tr>
						<td>{PHP.L.primebox_place}:</td>
						<td>{PRIMEBOX_FORM_CATEGORY}</td>
					</tr>
					<tr>
						<td>{PHP.L.primebox_expire_time}:</td>
						<td>{PRIMEBOX_FORM_PERIOD}</td>
					</tr>
          <!-- ENDIF -->
					<tr>
						<td>{PHP.L.Image}:</td>
						<td>
              <img src="{PRIMEBOX_FORM_IMAGE}" style="width:100%"/>
              {PRIMEBOX_FORM_FILE}
            </td>
					</tr>
					<tr>
						<td>{PHP.L.primebox_alt}:</td>
						<td>{PRIMEBOX_FORM_ALT}</td>
					</tr>
					<tr>
						<td>{PHP.L.primebox_click_url}:</td>
						<td>
              {PRIMEBOX_FORM_CLICKURL}
              <small>В формате http://mysite.ru</small>
            </td>
					</tr>
					<tr>
						<td>{PHP.L.Description}:</td>
						<td>{PRIMEBOX_FORM_DESCRIPTION}</td>
					</tr>   
          <!-- BEGIN: EXTRAFLD -->
					<tr>
						<td>{PRIMEBOX_FORM_EXTRAFLD_TITLE}:</td>
						<td>{PRIMEBOX_FORM_EXTRAFLD}</td>
					</tr>  
          <!-- END: EXTRAFLD -->      
					<tr>
						<td></td>
						<td><button class="btn btn-success">{PHP.L.Save}</button></td>
					</tr>
				</tbody>
      </table>
  <!-- END: EDIT -->
                        
  <!-- BEGIN: SHOW -->
    <h1>{PAGE_TITLE}
      <a href="{PHP|cot_url('plug', 'e=primebox')}#create" data-toggle="tab" class="btn btn-success pull-right">Создать</a>
    </h1>
		 {FILE "{PHP.cfg.themes_dir}/{PHP.cfg.defaulttheme}/warnings.tpl"}
      <div class="tab-content">  
       <div role="tabpanel" class="tab-pane fade" id="create">
        <form action="{PHP|cot_url('plug', 'e=primebox&a=edit&act=add')}" method="POST" ENCTYPE="multipart/form-data" class="form-horizontal">
        <table class="table">
					<tbody>
					<tr>
						<td>{PHP.L.Title}:</td>
						<td>{PRIMEBOX_FORM_TITLE}</td>
					</tr> 
					<tr>
						<td>{PHP.L.primebox_place}:</td>
						<td>{PRIMEBOX_FORM_CATEGORY}</td>
					</tr>
					<tr>
						<td>{PHP.L.primebox_expire_time}:</td>
						<td>{PRIMEBOX_FORM_PERIOD}</td>
					</tr> 
					<tr>
						<td>{PHP.L.Image}:</td>
						<td>{PRIMEBOX_FORM_FILE}</td>
					</tr> 
					<tr>
						<td>{PHP.L.primebox_alt}:</td>
						<td>{PRIMEBOX_FORM_ALT}</td>
					</tr> 
					<tr>
						<td>{PHP.L.primebox_click_url}:</td>
						<td>
               {PRIMEBOX_FORM_CLICKURL}
               <small>В формате http://mysite.ru</small>
            </td>
					</tr>
					<tr>
						<td>{PHP.L.Description}:</td>
						<td>{PRIMEBOX_FORM_DESCRIPTION}</td>
					</tr> 
          <!-- BEGIN: EXTRAFLD -->
					<tr>
						<td>{PRIMEBOX_FORM_EXTRAFLD_TITLE}:</td>
						<td>{PRIMEBOX_FORM_EXTRAFLD}</td>
					</tr>  
          <!-- END: EXTRAFLD -->      
					<tr>
						<td></td>
						<td><button class="btn btn-success">{PHP.L.primebox_publish}</button></td>
					</tr>
				</tbody>
       </table>
	    </form>
     </div>
    </div>
   
   <hr>
              
    <!-- IF {PRIMEBOX_COUNT} == 0 -->
      <p>Активных primebox нет.</p>
    <!-- ENDIF -->
              
    <!-- BEGIN: PRIMEBOX_ROW -->
      <div class="row"> 
        <div class="span12">
         <div class="well">
            <h4>{PRIMEBOX_ROW_TITLE}
                <!-- IF {PRIMEBOX_ROW_DEL_URL} -->
                 <a class="pull-right" href="{PRIMEBOX_ROW_DEL_URL}">{PHP.L.Delete}</a>
                <!-- ENDIF -->
               <a href="{PRIMEBOX_ROW_EDIT_URL}">{PHP.L.Edit}</a>
             </h4>

                <div class="span3">
                  <img src="{PRIMEBOX_ROW_IMAGE}" style="max-width:100%" alt="{PRIMEBOX_ROW_ALT}"/>
                </div>
                      
                       <div class="span5">
                          <h4>Место размещения</h4>
                          <p>{PRIMEBOX_ROW_CATEGORY_TITLE}</p>

                          <h4>Описание</h4>
                          <p>{PRIMEBOX_ROW_DESCRIPTION}</p>
                          
                          <h4>Alt текст</h4>
                          <p>{PRIMEBOX_ROW_ALT}</p>
                                                    
                          <h4>Url для перехода</h4>
                          <p>{PRIMEBOX_ROW_URL}</p>
                       </div>
                       
                       <div class="span3">
                          <h3>Статус:</h3>
                          <p>
                           Срок размещения {PRIMEBOX_ROW_PERIOD} <br>
                           <!-- IF {PRIMEBOX_ROW_EXPIRE} == 0 AND {PRIMEBOX_ROW_PAUSED} == 0 -->
                            <a class="btn btn-warning btn-sm" href="{PRIMEBOX_ROW_ID|cot_url('plug', 'e=primebox&act=buy&id='$this)}">Оплатить размещение</a>
                           <!-- ELSE -->
                             <!-- IF {PRIMEBOX_ROW_EXPIRE} > {PHP.sys.now} -->
                              Размещен до <b>{PRIMEBOX_ROW_EXPIRE|cot_date('d.m.Y',$this)}</b>
                               <br>
                              <a href="{PRIMEBOX_ROW_ID|cot_url('plug', 'e=primebox&act=paused&id='$this)}">Приостановить показ</a>
                             <!-- ELSE -->
                               <!-- IF {PRIMEBOX_ROW_EXPIRE} > 0 -->
                                Завершен <b>{PRIMEBOX_ROW_EXPIRE|cot_date('d.m.Y',$this)}</b
                               <!-- ENDIF -->
                               <!-- IF {PRIMEBOX_ROW_PAUSED} > 0 -->
                                Остановлен <b>(Остаток {PRIMEBOX_ROW_PAUSED_TIME})</b>
                                 <br>
                                <a href="{PRIMEBOX_ROW_ID|cot_url('plug', 'e=primebox&act=unpaused&id='$this)}">Возобновить показ</a>
                               <!-- ENDIF -->                            
                             <!-- ENDIF -->
                           <!-- ENDIF -->
                          </p>
                           
                    <h3>Статистика:</h3>
                      <p>Показов: <b>{PRIMEBOX_ROW_SHOWS}</b>
                         <br>
                         Кликов: <b>{PRIMEBOX_ROW_CLICKS}</b>
                      </p>
                      
                    </div>   
              <div class="clear"></div>
            </div>  
          </div>
        </div>       
      <!-- END: PRIMEBOX_ROW -->
                 
    <!-- END: SHOW -->
  </div>
</div>
<!-- END: MAIN -->