<?php

/* ====================
  [BEGIN_COT_EXT]
  Hooks=admin.extrafields.first
  [END_COT_EXT]
  ==================== */


defined('COT_CODE') or die('Wrong URL');

require_once cot_incfile('primebox', 'plug');
$extra_whitelist[$db_primebox] = array(
	'name' => $db_primebox,
	'caption' => $L['Plugin'].' primebox',
	'type' => 'plugin',
	'code' => 'primebox',
	'tags' => array()
);
