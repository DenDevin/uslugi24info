<?php

/* ====================
  [BEGIN_COT_EXT]
  Hooks=admin.structure.first
  [END_COT_EXT]
  ==================== */

defined('COT_CODE') or die('Wrong URL');

$extension_structure[] = 'primebox';
if ($n == 'primebox')
{
	require_once(cot_incfile('primebox', 'plug'));
	require_once cot_langfile('primebox', 'plug');

	$t = new XTemplate(cot_tplfile('primebox.admin.structure', 'plug'));
}
