<?php
/* ====================
[BEGIN_COT_EXT]
Code=onliner
Name=Online users widget
Category=community-social
Description=Виджет пользователей, которые онлайн на сайте<br /> [<a target="_blank" href="http://freelance-script.abuyfile.com/plugin-onliner/">Документация по плагину, помощь, поддержка</a>]
Version=1.0
Date=2013-05-17
Author=devkont
Copyright=CMSWorks Team 2010-2013
Notes=BSD License
SQL=
Auth_guests=
Lock_guests=RW12345A
Auth_members=
Lock_members=W12345A
Requires_plugins=Whosonline
[END_COT_EXT]

[BEGIN_COT_EXT_CONFIG]
disable_guests=01:radio::0:Disable guest tracking
css=99:radio:0,1:1:Enable plugin CSS
[END_COT_EXT_CONFIG]
==================== */

defined('COT_CODE') or die('Wrong URL');

?>