<?php
/**
 * [BEGIN_COT_EXT]
 * Code=paymarkettop
 * Name=PayMarketTop
 * Category=Payments
 * Description=Услуга "Закрепить товар"
 * Version=1.0.1
 * Date=
 * Author=CMSWorks Team
 * Copyright=Copyright (c) CMSWorks.ru
 * Notes=
 * Auth_guests=R
 * Lock_guests=12345A
 * Auth_members=RW
 * Lock_members=12345A
 * Requires_modules=payments,market
 * Requires_plugins=
 * [END_COT_EXT]
 * 
 * [BEGIN_COT_EXT_CONFIG]
 * cost=01:string::100:Стоимость за день размещения
 * [END_COT_EXT_CONFIG]
 */

/**
 * paymarkettop Plugin
 *
 * @package paymarkettop
 * @version 1.0.1
 * @author CMSWorks Team
 * @copyright Copyright (c) CMSWorks.ru
 * @license BSD
 */