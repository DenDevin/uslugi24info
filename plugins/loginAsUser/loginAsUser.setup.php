<?php
/* ====================
[BEGIN_COT_EXT]
Code=loginAsUser 
Name=loginAsUser 
Category=hidden
Description=Плагин позволяет админу заходить как пользователь, для того чтоб протестировать как будто это он.
Version=1.0.0
Date=08.05.2017
Author=PluginsPro Team
Copyright=Copyright (c) PluginsPro Team 2017
Notes=Private License
Auth_guests=
Lock_guests=RW12345A
Auth_members=R
Lock_members=W12345A
[END_COT_EXT]

[BEGIN_COT_EXT_CONFIG]

[END_COT_EXT_CONFIG]
==================== */

defined('COT_CODE') or die('Wrong URL');
