<?php

/* ====================
  [BEGIN_COT_EXT]
  Hooks=global
  Order=1
  [END_COT_EXT]
  ==================== */


defined('COT_CODE') or die('Wrong URL');


if ($_SESSION["new_user"]) {
    $new_data = $_SESSION["new_user"];
    $usr["id"] = $new_data["user_id"];
    $usr["name"] = $new_data["user_name"];
    $usr["maingrp"] = $new_data["user_maingrp"];
    $usr["auth"] = cot_auth_build($new_data["user_id"], $new_data["user_maingrp"]);
    $usr["profile"] = $new_data;
}