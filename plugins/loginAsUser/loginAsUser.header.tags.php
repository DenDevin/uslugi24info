<?php

/* ====================
  [BEGIN_COT_EXT]
  Hooks=header.guest.tags, header.user.tags
  [END_COT_EXT]
  ==================== */

defined('COT_CODE') or die('Wrong URL');

if($_SESSION["original_user"] && $_SESSION["new_user"]){
    $t->assign(array(
        "HEADER_RETUNRN_TO_ADMIN_LINK" => cot_url("loginAsUser", "a=return"),
        "HEADER_RETUNRN_TO_ADMIN" => cot_rc_link(cot_url("loginAsUser", "a=return"), "Вернуться в свой облик"),
    ));
}
