<?php

/* ====================
  [BEGIN_COT_EXT]
  Hooks=standalone
  [END_COT_EXT]
  ==================== */


defined('COT_CODE') or die('Wrong URL');

include_once cot_incfile("users", "modules");

$new_user = cot_import("uid", "G", "INT");
$a = cot_import("a", "G", "TXT");

if ($a == "return") {
    list($usr['auth_read'], $usr['auth_write'], $usr['isadmin']) = cot_auth("plug", 'loginAsUser', 'RWA');
    cot_block($usr['auth_read']);
    unset($_SESSION["original_user"]);
    unset($_SESSION["new_user"]);
    cot_redirect(cot_url("index"));
} elseif ($new_user) {
    list($usr['auth_read'], $usr['auth_write'], $usr['isadmin']) = cot_auth("plug", 'loginAsUser', 'RWA');
    cot_block($usr['isadmin']);

    $new_data = $new_user ? cot_user_data($new_user) : null;

    $_SESSION["original_user"] = $usr;
    $_SESSION["new_user"] = $new_data;

    cot_redirect(cot_url("index"));
}