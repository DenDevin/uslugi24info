<?php
/* ====================
[BEGIN_COT_EXT]
Code=autoalias2lance
Name=AutoAlias 2 Freelance
Category=navigation-structure
Description=Генерирование алиаса(кода URL) страницы, для модулей ЗАКАКЗЫ, УСЛУГИ, ПОРТФОЛИО, если пользователь не заполнил поле, при добавлении публикации в каталог
Version=1.0.0
Date=2015-03-09
Author=
Copyright=
Notes=BSD License
Auth_guests=R
Lock_guests=W12345A
Auth_members=RW
Lock_members=12345
Recommends_modules=market,folio,projects
Requires_plugins=autoalias2
[END_COT_EXT]

[BEGIN_COT_EXT_CONFIG]
fl_projects_alias=01:radio:1,0:1:
fl_market_alias=02:radio:1,0:1:
fl_folio_alias=03:radio:1,0:1:
[END_COT_EXT_CONFIG]
==================== */

defined('COT_CODE') or die('Wrong URL');