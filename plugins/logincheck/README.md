# cot-logincheck

Дата публикации:
14.06.2013
Версия:
1.4
Обновление:
13.03.2015
Фреймворк:
Cotonti Siena


Простой плагин для проверки правильности введенного логина при регистрации пользователя. Плагин разрешает указывать в логине только латинские символы и цифры при регистрации пользователя. Также плагин умеет проверять логин на запрещенные имена.

 

Как установить и настроить плагин:

    Распакуйте и скопируйте папку logincheck в директорию plugins/ вашего сайта на Cotonti;
    Зайдите в админ-панель сайта и перейдите в раздел "Расширения". Установите плагин Logincheck.
    В настройках плагина при необходимости можно указать список запрешенных логинов (через запятую).


Плагин проверки логина при регистрации для Cotonti Siena

Страница разработки: http://cmsworks.ru/catalog/plugins/logincheck

Разработчик: Булат Юсупов support@cmsworks.ru

Copyright CMSWorks Team 2015
