# PLUGIN «MYADS» FOR COTONTI

 Roman Roffun edited this page on 27 Dec 2016 · 


Cotonti плагин myads - расширение с помощью которого можно выводить рекламные блоки в разных частях сайта, как в коде шаблона, так и в теле статьи. По умолчанию предусмотрено 8 рекламных мест в шаблоне, и 5 мест под рекламные блоки в теле статьи. Внешние скрипты вызова можно полключить как в верхней части, так и в нижней. Для этого есть специальные поля.
Установка плагина myads

        Скачать плагин, распаковать.
        Сам плагин (папка myads внутри архива) заливается в корневой каталог /plugins/. 
        Перейти в Управление сайтом / Расширения / myads и установить.
        Добавить в шаблон приведенный ниже код.

Вывод рекламных блоков в верхней части шаблона

в header.tpl:

<!-- IF {PHP.myads_header} -->{PHP.myads_header}<!-- ENDIF -->

 
Вывод рекламных блоков в нижней части шаблона

в index.tpl верхнее рекламное место:

<!-- IF {PHP.myads_main_top} -->{PHP.myads_main_top}<!-- ENDIF -->

в index.tpl нижнее рекламное место:

<!-- IF {PHP.myads_main_bottom} -->{PHP.myads_main_bottom}<!-- ENDIF -->

 
Вывод рекламных блоков в левой боковой части шаблона

в sidebars.tpl левый сайдбар, верхнее рекламное место:

<!-- IF {PHP.myads_sideleft_top} -->{PHP.myads_sideleft_top}<!-- ENDIF -->

в sidebars.tpl левый сайдбар, нижнее рекламное место:

<!-- IF {PHP.myads_sideleft_bottom} -->{PHP.myads_sideleft_bottom}<!-- ENDIF -->

 
Вывод рекламных блоков в правой боковой части шаблона

в sidebars.tpl правый сайдбар, верхнее рекламное место:

<!-- IF {PHP.myads_sideright_top} -->{PHP.myads_sideright_top}<!-- ENDIF -->

в sidebars.tpl правый сайдбар, нижнее рекламное место:

<!-- IF {PHP.myads_sideright_bottom} -->{PHP.myads_sideright_bottom}<!-- ENDIF -->

 
Вывод рекламных блоков в нижней части шаблона

в footer.tpl

<!-- IF {PHP.myads_footer} -->{PHP.myads_footer}<!-- ENDIF -->

 

К каждому полю можно сделать пометку, в каком блоке какой рекламный код. Для этого можно добавить описание блоков шаблона, через запятую по порядку. После сохранения каждое описание появится согласно добавлению.
Вывод рекламы в теле статьи

Для того чтобы иметь возможность вставить рекламу в тело статьи, нужно обладать правами. В настройках плагина через запятую перечисляются ID пользователей, которым разрешено вставлять теги вызова рекламных блоков в текст. Для блоков пометку также можно сделать в специальном поле. Чтобы вставлять блоки по клику на одну из пяти кнопок в CKEditor, нужно добавить следующий код:

в page.add.tpl и page.edit.tpl:

<!-- IF {PHP|myads_usersdone} == 'myads_done' AND {PHP.cfg.parser} == 'html' AND {PHP.cfg.plugin.html.editor} == 'ckeditor' -->		
                       <div>   		
                        <script>		
                              function InsertBanner(banner) {		
                             var editor = CKEDITOR.instances['rpagetext'];		
                             if ( editor.mode == 'wysiwyg' )		
                              {editor.insertHtml('[SCEBANNER'+banner+']' );}		
                             else alert( '{PHP.L.myads_visy}' );}		
                         </script>		
                             <a href="javascript:InsertBanner('1')" class="btn" title="{PHP.cdesc.0}">{PHP.L.myads_advblock} 1</a>		
                             <a href="javascript:InsertBanner('2')" class="btn" title="{PHP.cdesc.1}">{PHP.L.myads_advblock} 2</a>		
                             <a href="javascript:InsertBanner('3')" class="btn" title="{PHP.cdesc.2}">{PHP.L.myads_advblock} 3</a>		
                             <a href="javascript:InsertBanner('4')" class="btn" title="{PHP.cdesc.3}">{PHP.L.myads_advblock} 4</a>		
                             <a href="javascript:InsertBanner('5')" class="btn" title="{PHP.cdesc.4}">{PHP.L.myads_advblock} 5</a>		
                       </div>		
<!-- ENDIF -->

 

 
Copyright (C) 2014 - today: Roffun | https://github.com/Roffun
