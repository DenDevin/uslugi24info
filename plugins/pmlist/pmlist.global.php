<?php defined('COT_CODE') or die('Wrong URL');
/* ====================
[BEGIN_COT_EXT]
Hooks=global
[END_COT_EXT]
==================== */

/*
#s - статус соощений
0 - new message
1 - inbox message
2 - starred message
3 - deleted message
--------------------
$l - количество сообщений
-------------------
*/
function cot_pmlist($l=10, $s=0){
	global $db_pm, $db, $cfg, $db_users, $usr;

	if($usr['id'] < 1){
		return false;
	}
	$pm_sql = $db->query("SELECT p.*, u.* FROM $db_pm AS p
			LEFT JOIN $db_users AS u
			ON u.user_id = p.pm_fromuserid
			WHERE pm_touserid = '".$usr['id']."' AND pm_tostate <> 3 AND pm_tostate = {$s}
			ORDER BY pm_date DESC LIMIT 0, {$l}")->fetchAll();

	$tt = new XTemplate(cot_tplfile('pmlist', 'plug'));

	// Display the list

	foreach ($pm_sql as $key => $value) {
		$tt->assign(array(
			'PMLIST_ID'    	=> $value['pm_id'],
			'PMLIST_TEXT' 	=> cot_parse($value['pm_text'], $cfg['pm']['markup']),
			'PMLIST_TITLE' 	=> $value['pm_title'],
			'PMLIST_URL' 	=> cot_url('pm', 'm=message&id='.$value['pm_id'])
		));
		$tt->assign(cot_generate_usertags($value['pm_fromuserid'], 'PMLIST_FROM_USER_'));
		$tt->parse('MAIN.LIST');
	}

	$tt->parse('MAIN');
	 return $tt->text('MAIN');
}
