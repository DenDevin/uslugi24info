<!-- BEGIN: MAIN -->
	<ul>
		<!-- BEGIN: LIST -->
			<li>
				<a href="{PMLIST_URL}">
					<p>#{PMLIST_ID}. {PMLIST_TITLE}</p>
					<div class="muted">{PMLIST_TEXT|cot_string_truncate($this,150)}</div>
				</a>
			</li>	
		<!-- END: LIST -->
	</ul>
<!-- END: MAIN -->
