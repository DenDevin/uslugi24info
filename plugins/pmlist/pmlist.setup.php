<?php defined('COT_CODE') or die('Wrong URL');
/* ====================
[BEGIN_COT_EXT]
Code=pmlist
Name=PM list
Description=List private mesage
Version=1.0.0
Date=2015-09-23
Author=CrazyFreeMan
Copyright=&copy; CrazyFreeMan, 2015
Notes=Global {PHP|cot_pmlist([limit int],[state int])}, From user tags - {PMLIST_FROM_USER_***}
Auth_guests=R
Lock_guests=2345A
Auth_members=RW
Lock_members=2345
Requires_modules=pm
Requires_plugins=
Recommends_modules=
Recommends_plugins=
[END_COT_EXT]
==================== */
