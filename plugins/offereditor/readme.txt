Плагин добавляет возможность фрилансеру или администратору управлять предложением к проекту: редактировать, отменять или удалять.

Инструкция по установке

    Распакуйте исходники в папку plugins вашего сайта.
    Зайдите в панель администратора и установите данный плагин.
    Скопируйте шаблон из биржи modules/projects/tpl/projects.offers.tpl в свою тему и добавьте следующий код (в список предложений между <!-- BEGIN: ROWS -->...<!-- END: ROWS -->):

    	
    <!-- IF {PHP.cot_plugins_active.offereditor} -->
     
        <!-- IF {PHP.usr.id} == {OFFER_ROW_OWNER_ID} OR {PHP.usr.isadmin} -->
        <div class="pull-right">
            <a href="{OFFER_ROW_OFFEREDITOR_EDIT_URL}" class="ajax" rel="get-offers">{PHP.L.offereditor_edit}</a> |
            <!-- IF {OFFER_ROW_STATUS} != 'canceled' --><a href="{OFFER_ROW_OFFEREDITOR_CANCEL_URL}">{PHP.L.offereditor_cancel}</a> | <!-- ENDIF -->
            <a href="{OFFER_ROW_OFFEREDITOR_DELETE_URL}">{PHP.L.offereditor_delete}</a>
        </div>
        <!-- ENDIF -->
     
        <!-- IF {OFFER_ROW_STATUS} == 'canceled' -->
        <div class="text-warning">{PHP.L.offereditor_status_canceled}</div>
        <!-- ENDIF -->
     
    <!-- ENDIF -->

    Обращаю внимание, чтобы все работало правильно список предложений должен быть обрамлен в div с id="offers", как это сделано в исходной сборке биржи.
