<!-- BEGIN: MAIN -->
<ul class="uk-list uk-list-striped">
<!-- BEGIN: PAGE_ROW -->
    <li>
	<article class="uk-article">
	    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-8-10">
        <h1 class="uk-h2"> <a href="{PAGE_ROW_URL}" title="{PAGE_ROW_SHORTTITLE}" data-toggle="tooltip">{PAGE_ROW_SHORTTITLE|cot_cutstring($this,120)}</a>
        </h1>
<!-- 		<p>
            {PAGE_ROW_TEXT|cot_cutstring($this,70)}
        </p> -->
                        <!-- IF {PAGE_ROW_DESC} -->
                        <p class="uk-text-small">
                            {PAGE_ROW_DESC|cot_cutstring($this,300)}
                        </p>
                        <!-- ENDIF -->
		</div>
		<div class="uk-width-2-10">
        <div class="uk-grid">
		<div class="uk-width-medium-1-2">
		<p class="uk-float-right">
		<!-- IF {PHP.cot_plugins_active.ratings} -->
        {PAGE_ROW_RATINGS_DISPLAY}&nbsp;({PAGE_ROW_RATINGS_COUNT})
        <!-- ENDIF --></p>
		</div>
		<div class="uk-width-medium-1-2">
		<p class="uk-float-right">{PAGE_ROW_DATE_STAMP|cot_date('date_full', $this)}</p>
		</div>
		</div>
		</div>

	</article>
    </li>
<!-- END: PAGE_ROW -->
</ul>
<!-- BEGIN: NO_PAGES_FOUND -->
<!-- <div class="centerall">{PHP.L.recentitems_nonewpages}</div> -->
<!-- END: NO_PAGES_FOUND -->
<!-- END: MAIN -->