<?php

/* ====================
  [BEGIN_COT_EXT]
  Hooks=rc
  [END_COT_EXT]
  ==================== */

defined('COT_CODE') or die('Wrong URL');

cot_rc_add_file($cfg['plugins_dir'] . '/catbox/css/catbox.css');

cot_rc_link_footer($cfg['plugins_dir'] . '/catbox/js/catbox.js');
