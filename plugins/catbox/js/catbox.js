$(function () {
    $('[data-catbox="true"]').click(function () {
        var catName = $(this).attr("data-name");
        if ($('[data-catbox-name="' + catName + '"]').css('display') == "none") {
            $('[data-catbox-name="' + catName + '"]').show("slide", {direction: "left"}, 300);
        } else {
            $('[data-catbox-name]').hide("slide", {direction: "left"}, 300);
            $('[data-catbox-name] ul').hide("slide", {direction: "left"}, 300);
        }
    });
    $('[data-catbox-name] a').click(function () {
        var target = $(this).data("target");

        if ($(this).data('spec') == $("input[name='" + target + "']").val() && $(this).next("ul").css('display') == "block") {
            $('.catbox-list').hide("slide", {direction: "left"}, 300);
        }
        if ($(this).next("ul").length == 0) {
            $('.catbox-list').hide("slide", {direction: "left"}, 300);
        }

        $("input[name='" + target + "']").val($(this).data('spec'));
        $("input[name='" + target + "_title']").val($(this).text());
        $(this).parent().parent().find('ul').hide("slide", {direction: "left"}, 300);
        $(this).next("ul").show("slide", {direction: "left"}, 300);

        return false;
    });
});
