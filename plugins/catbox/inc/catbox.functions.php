<?php

defined('COT_CODE') or die('Wrong URL.');

function cot_catbox($area, $parent = '', $name, $level = 0, $attr = '', $uclass = '') {
    global $structure;
    $title = cot_structure_buildpath($area, $parent);
    $kk = count($title)-1;
    
    require_once cot_incfile('forms');
    $html = cot_inputbox("hidden", $name, $parent);
    $html .= cot_inputbox("text", $name . '_title', $title[$kk][1], 'autocomplete="off" data-catbox="true" data-name="'.$name.'" ' . $attr);
    $html .= cot_catbox_list($area, $p, $name, $level, 'data-catbox="true" data-name="'.$name.'" ' . $attr, $uclass);

    return $html;
}

function cot_catbox_list($area, $parent = '', $name, $level = 0, $attr = '', $uclass = '') {
    global $structure, $cfg, $db, $sys, $type;

    if (empty($parent)) {
        $children = array();
        foreach ($structure[$area] as $i => $x) {
            if (mb_substr_count($structure[$area][$i]['path'], ".") == 0) {
                $children[] = $i;
            }
        }
    } else {
        $children = cot_structure_children($area, $parent, false, false);
    }

    if (count($children)) {
        if (empty($parent)) {
            $html = "<ul data-catbox-name='{$name}' class='catbox-list {$uclass}'>";
        } else {
            $html = "<ul class='catbox-list {$uclass}'>";
        }
        foreach ($children as $k => $v) {

            $mtch = $structure[$area][$v]['path'] . '.';
            $mtchlvl = mb_substr_count($mtch, ".");

            $id = explode(".", $structure[$area][$v]["path"]);
            $p = explode(".", $structure[$area][$v]["path"]);
            $title = $structure[$area][$v]["title"];

            $html .= '<li>';
            $html .= '<a href="#" data-spec="' . $id[$mtchlvl - 1] . '" data-target="' . $name . '">' . $title . '</a>';
            $html .= cot_catbox_list($area, $p[$mtchlvl - 1], $name, $level, $attr, $uclass);
            $html .= '</li>';
        }
        $html .= "</ul>";
    }
    return $html;
}
