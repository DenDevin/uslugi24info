<!-- BEGIN: FOOTER -->
</div>
<div class="uk-card uk-card-default uk-card-large uk-card-body uk-margin-large-top tp-footer">
<div class="uk-container">
    <div uk-grid>
        <div class="uk-width-expand@m">
        	
        </div>
        <div class="uk-width-auto@m">
            <h3 class="uk-card-title uk-text-uppercase">{PHP.cfg.maintitle}</h3>
            <div id="footer">&copy; 2015 - {PHP|cot_date('Y')} <a href="{PHP|cot_url('admin')}">{PHP.cfg.maintitle}</a></div>
        </div>
    </div>
</div>
</div>
<a class="uk-totop uk-icon tp-totop" href="#tp-top" uk-totop="" uk-scroll></a>

{FOOTER_RC}
</body>
</html>
<!-- END: FOOTER -->