<!-- BEGIN: MAIN -->
<script src="{PHP.cfg.modules_dir}/demo/js/demo.admin.js" type="text/javascript"></script>	
<div id="catgenerator" style="display:none">
    <table class="uk-table uk-table-divider">
        <tr>
            <th>Название</th>
            <th>Ссылка на демо</th>
            <th>Ссылка на купить</th>
            <th>&nbsp;</th>
        </tr>
        <tr class="newscat" style="display: none;">
            <td><input type="text" class="uk-input ca_ext" name="ca_ext" value="" maxlength="255" /></td>
            <td><input type="text" class="uk-input ca_cat" name="ca_cat" value=""  maxlength="255" /></td>
            <td><input type="text" class="uk-input ca_path" name="ca_path" value=""  maxlength="255" /></td>
            <td class="uk-text-center"><a href="#" class="deloption negative uk-text-danger uk-button uk-button-default uk-button-small" style="display:none">{PHP.L.Delete}</a></td>
        </tr>
        <!-- BEGIN: ADDITIONAL -->
        <tr class="newscat">
            <td><input type="text" class="uk-input uk-input ca_ext" name="ca_ext" value="{ADDMODULE}"  maxlength="255" /></td>
            <td><input type="text" class="uk-input ca_cat" name="ca_cat" value="{ADDCATEGORY}"  maxlength="255" /></td>
            <td><input type="text" class="uk-input uk-input ca_path" name="ca_path" value="{ADDPATH}"  maxlength="255" /></td>
            <td class="uk-text-center"><a href="#" class="deloption negative uk-text-danger uk-button uk-button-default uk-button-small" style="display:none"">{PHP.L.Delete}</a></td>
        </tr>
        <!-- END: ADDITIONAL -->
        <tr id="addtr">
            <td class="uk-text-center" colspan="8">
            <button name="addoption" id="addoption" type="button" class="uk-button uk-button-default" >{PHP.L.Add}</button>
            </td>
        </tr>
    </table>
</div>
<!-- END: MAIN -->