<!-- BEGIN: MAIN -->
<!-- BEGIN: IMPORT -->
<div class="uk-card uk-card-default uk-card-body uk-margin-top">
    <table class="uk-table uk-table-divider">
        <thead>
            <tr>
                <th class="uk-text-center">Импортировать сообщения из модуля Private Messages?</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="uk-text-center"><a class="uk-button uk-button-default" href="{IMPORT_PM}">Начать импорт</a></td>
            </tr>
        </tbody>
    </table>
</div>
<!-- END: IMPORT -->
<!-- BEGIN: SUCCESS -->
<div class="uk-alert-success" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    Успешно импортировано {IMPORT_PM} сообщений.
</div>
<!-- END: SUCCESS -->
<!-- BEGIN: ERROR -->
<div class="uk-alert-danger" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    Произошла ошибка. Отсутствуют сообщения.
</div>
<!-- END: ERROR -->
<!-- BEGIN: ERROR2 -->
<div class="uk-alert-danger" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    Произошла ошибка. Не установлен модуль PrivateMassages.
</div>
<!-- END: ERROR2 -->
<hr />
<!-- END: MAIN -->