<!-- BEGIN: MAIN -->
<div class="uk-card uk-card-default uk-card-body uk-margin-top">
    <!-- BEGIN: MSG_ROW_EMPTY -->
    <div  class="uk-alert uk-alert-danger"  id="error" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    {PHP.L.None}
    </div>
    <!-- END: MSG_ROW_EMPTY -->
    <div class="ds_scroll uk-overflow-auto">
        <table class="uk-table uk-table-divider">
            <!-- BEGIN: MSG_ROW -->
            <tr>
                <td style="width: 5%;" class="ds_avatar">{MSG_ROW_USER_AVATAR}</td>            
                <td id="{MSG_ROW_ID}" style="width: 85%;<!-- IF {MSG_ROW_READSTATUS} --> background: #F3F3F3;<!-- ENDIF -->">
                    <span class="pull-right">{MSG_ROW_USER_MAINGRP} {MSG_ROW_DATE}</span>
                    {MSG_ROW_USER_NAME}<br />
                    <span msg="edit" class="ds_edit<!-- IF !{MSG_ROW_READSTATUS} --> read<!-- ENDIF -->" id="{MSG_ROW_ID}" href="{MSG_EDIT_URL}">{MSG_ROW_TEXT}
                    </span>
                </td>
            </tr>
            <!-- END: MSG_ROW -->
        </table>    
    </div>    
</div>

<!-- END: MAIN -->