<!-- BEGIN: MAIN -->

<!-- BEGIN: CHAT_DELETE -->
    <div class="uk-alert-danger" uk-alert>
    <a class="uk-alert-close" uk-close></a>
      <span>Вы действительно хотите удалить переписку между пользователями {MSG_FROM_USER_NAME} и {MSG_TO_USER_NAME}? <br />
      <a href="{MSG_DELETE}">Да</a>
      </span>
    </div>
<!-- END: CHAT_DELETE -->

<!-- BEGIN: DELETE_SUCCESS -->
    <div class="uk-alert-success" uk-alert>
    <a class="uk-alert-close" uk-close></a>
      <strong>Успешно!</strong> Удалено {MSG_JJ} сообщений.
    </div>
<!-- END: DELETE_SUCCESS -->

<!-- END: MAIN -->