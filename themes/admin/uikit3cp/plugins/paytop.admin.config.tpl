<!-- BEGIN: MAIN -->
<script src="{PHP.cfg.plugins_dir}/paytop/js/paytop.admin.config.js" type="text/javascript"></script>	
<div id="areagenerator" style="display:none">
    <table class="uk-table uk-table-divider">
        <tr>
            <td>{PHP.L.paytop_admin_config_area}</td>
            <td>{PHP.L.paytop_admin_config_name}</td>
            <td>{PHP.L.paytop_admin_config_cost}</td>
            <td>{PHP.L.paytop_admin_config_period}</td>
            <td>{PHP.L.paytop_admin_config_count}</td>
            <td>&nbsp;</td>
        </tr>
        <!-- BEGIN: ADDITIONAL -->
        <tr class="area">
            <td>{ADDAREA}</td>
            <td>{ADDNAME}</td>
            <td>{ADDCOST}</td>
            <td>{ADDPERIOD}</td>
            <td>{ADDCOUNT}</td>
            <td><a href="#" class="deloption negative button"><span class="trash icon"></span>{PHP.L.Delete}</a></td>
        </tr>
        <!-- END: ADDITIONAL -->
        <tr id="addtr">
            <td class="uk-text-center" colspan="8"><button class="uk-button uk-button-default" name="addoption" id="addoption" type="button">{PHP.L.Add}</button></td>
        </tr>
    </table>
</div>

<!-- END: MAIN -->