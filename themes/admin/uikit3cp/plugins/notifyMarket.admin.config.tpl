<!-- BEGIN: MAIN -->
<script src="{PHP.cfg.plugins_dir}/notifyMarket/js/notifyMarket.admin.config.js" type="text/javascript"></script>	
<div id="groupgenerator" style="display:none">
    <!-- BEGIN: ADDITIONAL -->
    <div class="group">
        {ADDGROUP}
        <a href="#" class="deloption negative uk-link uk-text-danger">{PHP.L.Delete}</a>
    </div>
    <!-- END: ADDITIONAL -->
    <div id="addtr"><button name="addoption" id="addoption" type="button" class="uk-button uk-button-small uk-margin-top">{PHP.L.Add}</button></div>
</div>
<!-- END: MAIN -->