<!-- BEGIN: MAIN -->

<!-- BEGIN: PAYMENTS -->
<h2 class="uk-heading-primary uk-text-center uk-text-uppercase">{PHP.L.affiliate_payments_title}</h2>
<hr class="uk-divider-icon">
<p>{PHP.L.payments_allpayments}: <span class="uk-label uk-label-success">{PAYMENT_SUMM|number_format($this, '2', '.', ' ')} {PHP.cfg.payments.valuta}</span></p>	
<div class="uk-card uk-card-default uk-card-hover uk-card-body uk-margin-large-bottom">
    <table class="uk-table uk-table-divider uk-tabel-small uk-tabel-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>{PHP.L.Date}</th>
                <th>{PHP.L.affiliate_referal}</th>
                <th>{PHP.L.affiliate_partner}</th>
                <th class="uk-text-right">{PHP.L.affiliate_partner_summ}</th>
            </tr>
        </thead>
        <tbody>
            <!-- BEGIN: PAY_ROW -->
            <tr>
                <td><span class="uk-label uk-label-primary">{PAY_ROW_ID}</span></td>
                <td>{PAY_ROW_PDATE|cot_date('d.m.Y H:i', $this)}</td>
                <td>{PAY_ROW_REFERAL_NAME}</td>
                <td>{PAY_ROW_PARTNER_NAME}</td>
                <td class="uk-text-right"><span class="uk-label uk-label-success">{PAY_ROW_SUMM|number_format($this, '2', '.', ' ')} {PHP.cfg.payments.valuta}</td></span>
            </tr>
            <!-- END: PAY_ROW -->
        </tbody>
    </table>
</div>
<!-- END: PAYMENTS -->

<div class="uk-child-width-1-2@s uk-child-width-1-4@m" uk-grid>  
<!-- BEGIN: REF_ROW -->
<div>
    <div class="uk-card uk-card-default">
        <div class="uk-card-header">
            <div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-auto">
                    <!-- IF {REF_ROW_AVATAR_SRC} -->
                    <img class="uk-border-circle" width="40" height="40" title="{REF_ROW_NICKNAME}" alt="{REF_ROW_NICKNAME}" src="{REF_ROW_AVATAR_SRC}">
                    <!-- ELSE -->
                    <img class="uk-border-circle" width="40" height="40" title="{REF_ROW_NICKNAME}" alt="{REF_ROW_NICKNAME}" src="datas/defaultav/blank.png">
                    <!-- ENDIF -->
                </div>
                <div class="uk-width-expand">
                    <h3 class="uk-card-title uk-margin-remove-bottom">{REF_ROW_NICKNAME}</h3>
                    <p class="uk-text-meta uk-margin-remove-top"><time>{REF_ROW_REGDATE_STAMP|cot_date('j F Y', $this)}</time></p>
                </div>
            </div>
        </div>
        <div class="uk-card-body uk-padding-small">
           <ul class="uk-list">
           {REF_ROW_TREE}
           </ul>
        </div>
        <div class="uk-card-footer">
            <a href="{REF_ROW_DETAILSLINK}" class="uk-button uk-button-text">Read more</a>
        </div>
    </div>
</div>
<!-- END: REF_ROW -->
</div>
<!-- END: MAIN -->