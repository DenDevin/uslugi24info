<!-- BEGIN: MARKET -->
<div class="uk-grid uk-grid-match" data-uk-grid-match="{target:'.uk-panel'}">

    <!-- BEGIN: PRD_ROWS -->
    <div  class="uk-width-small-1-2 uk-width-medium-1-3" >
        <div class="uk-panel tp-card-default tp-card-hover">
		<a class="" href="{PRD_ROW_URL}">
            <div class="uk-card-media-top">
                <!-- IF {PRD_ROW_MAVATAR.1} -->
                <div class="uk-margin-bottom-remove">
                    <img width="500" height="350" alt="{PRD_ROW_SHORTTITLE}" src="{PRD_ROW_MAVATAR.1|cot_mav_thumb($this, 500, 350, crop)}" />
                </div>
                <!-- ELSE -->
                <img width="500" height="350" alt="" src="themes/{PHP.theme}/img/no-img.png">
                <!-- ENDIF -->
            </div>
            <div class="tp-card-body">
                <h3 class="tp-card-title uk-margin-remove"><a class="" data-uk-tooltip title="{PRD_ROW_SHORTTITLE}" href="{PRD_ROW_URL}">{PRD_ROW_SHORTTITLE|cot_string_truncate($this, 30, true, false, '...')}</a></h3>
                <span class="uk-text uk-text-bold uk-text-warning uk-margin-right">{PRD_ROW_CATTITLE}</span>
                <span class="uk-text uk-text-break">{PRD_ROW_TEXT|strip_tags($this)|mb_substr($this, 0, 160, 'UTF-8')}...</span>
                <div class="uk-grid uk-margin-top" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2 uk-text-truncate uk-text-center-small">
                        <span class="uk-width-1-1 uk-button uk-button-success">{PRD_ROW_COST} {PHP.cfg.payments.valuta}</span>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <a class="uk-width-1-1 uk-button uk-button-primary" href="{PRD_ROW_URL}">Купить</a>
                    </div>
                </div>
            </div>
			</a>
        </div>
    </div>
    <!-- END: PRD_ROWS -->
</div>
<!-- END: MARKET -->