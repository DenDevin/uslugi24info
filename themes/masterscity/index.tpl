<!-- BEGIN: MAIN -->
<!-- DESKTOP video-block -->
<div class="uk-cover uk-position-relative">
	<div class="overlay stripped mono"></div>
		<img class="uk-invisible" src="images/placeholder_600x400.svg" alt="" width="600" height="400" style=" background-attachment: fixed; min-height: 600px;">
		<video class="uk-cover-object uk-position-absolute" autoplay="" loop="" muted="" controls="" width="600" height="400" style=" background-attachment: fixed; min-height: 600px;">
			<source src="themes/{PHP.theme}/img/bg-video.mp4" type="video/mp4">
		</video>

                            
		<div class="uk-hidden-small uk-position-cover uk-padding-large uk-text-center uk-flex uk-flex-center uk-flex-top uk-margin-large-top" style="z-index:1">
			<div class="uk-margin-small uk-animation-scale-up" style="background: rgba(0,0,0,.7);">
			<h1 class="tp-text-transform uk-text-bold uk-text-contrast uk-margin-large-left uk-margin-large-right"><span class="tp-text-warning">«{PHP.cfg.maintitle}»</span></h1>
			<h4 class="tp-text-transform uk-text-bold uk-text-contrast uk-margin-large-left uk-margin-large-right">{PHP.cfg.subtitle}</h4>
				<form class="uk-margin-small-bottom uk-search" action="{PHP|cot_url('plug', 'e=search')}" method="post" data-uk-search="">
				<input type="hidden" name="e" value="market" />
					<input class="uk-form-large uk-width-1-1 uk-search-field" type="text" name="sq" placeholder="{PHP.L.Search}...">
				</form>
			</div>
		</div>
		<div class="uk-visible-small uk-position-cover uk-padding-large uk-text-center uk-flex uk-flex-center uk-flex-top uk-margin-large-top">
			<div class="uk-margin-small uk-animation-scale-up" style="background: rgba(0,0,0,.7); z-index:1">
			<h3 class="tp-text-transform uk-text-bold uk-text-contrast uk-margin-large-left uk-margin-large-right"><span class="tp-text-warning">«{PHP.cfg.maintitle}»</span></h3>
			<h6 class="tp-text-transform uk-text-bold uk-text-contrast uk-margin-large-left uk-margin-large-right">{PHP.cfg.subtitle}</h6>
					<form class="uk-margin-small-bottom uk-search" action="{PHP|cot_url('plug', 'e=search')}" method="post" data-uk-search="">
					<input type="hidden" name="e" value="market" />
						<input class="uk-form-large uk-width-1-1 uk-search-field" type="text" name="sq" placeholder="{PHP.L.Search}...">
					</form>
			</div>
		</div>
    <div class="uk-block tp-bottom-bloc"  style="z-index:1">
        <div class="uk-container uk-container-center">
            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-medium-3-4">
                    <!-- IF {PHP.usr.id} == 0 -->
					<h2 class="uk-hidden-small tp-text-transform uk-text-bold uk-text-contrast">{PHP.cfg.subtitle}</h2>
					<!-- <h2 class="uk-hidden-small tp-text-transform uk-text-bold uk-text-contrast">Готовый бизнес-сайт по частным и бизнес услугам.</h2> -->
					<!-- ENDIF -->
                </div>
                <div class="uk-width-medium-1-4 uk-margin-small-top">
                    <!-- IF {PHP.usr.maingrp} == 7 -->
                    <a class="tp-button uk-button-transparent tp-text-transform uk-button uk-button-large uk-width-1-1" href="{PHP|cot_url('projects', 'm=add')}">{PHP.L.projects_add_to_catalog}</a>
                    <!-- ENDIF -->
					<!-- IF {PHP.usr.maingrp} == 4 OR {PHP.usr.maingrp} == 5 OR {PHP.usr.maingrp} == 8 -->
					<a class="tp-button uk-button-transparent tp-text-transform uk-button uk-button-large uk-width-1-1" href="{PHP|cot_url('market', 'm=add')}">{PHP.L.market_add_product}</a>
					<!-- ENDIF -->
                    <!-- IF {PHP.usr.id} == 0 -->
                    <a class="tp-button uk-button-transparent tp-text-transform uk-button uk-width-1-1" href="{PHP.cot_groups.4.alias|cot_url('users', 'm=register&usergroup='$this)}">{PHP.L.I_am_an_freelancer}</a>
					<p><a class="tp-button uk-button-transparent tp-text-transform uk-button uk-width-1-1" href="{PHP.cot_groups.8.alias|cot_url('users', 'm=register&usergroup='$this)}">{PHP.L.I_am_an_company}</a></p>
                    <p><a class="tp-button uk-button tp-text-transform uk-width-1-1" href="{PHP.cot_groups.7.alias|cot_url('users', 'm=register&usergroup='$this)}">{PHP.L.I_am_an_employer}</a></p>
                    <!-- ENDIF -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END DESKTOP video-block -->

{FILE "themes/{PHP.theme}/inc/index.promo.steps.tpl"}

{FILE "themes/{PHP.theme}/inc/index.projects.cats.tpl"}

<!-- РАЗДЕЛ родительских категорий витрины услуг -->
{FILE "themes/{PHP.theme}/inc/index.market.cats.tpl"}
<!-- РАЗДЕЛ родительских категорий витрины услуг  КОНЕЦ--> 


<!-- блок лендинга степов (1-2-3) --> 
<div class="uk-block uk-block-muted">
	<div class="uk-container uk-container-center uk-margin-large-top">
        <div class="uk-container uk-container-center uk-text-center uk-hidden-small">
            <h2 class="uk-h1"><span class="tp-text-warning tp-text-transform uk-text-bold">«{PHP.cfg.maintitle}»</span> </br> {PHP.cfg.subtitle}</h2>
			<h3>С нашим сервисом вы экономите на услугах до 50%.<br>
            Как это возможно?</h3>
        </div>
		<div class="uk-container uk-container-center uk-text-center uk-visible-small">
            <h4><span class="tp-text-warning tp-text-transform uk-text-bold">«{PHP.cfg.maintitle}»</span> </br>{PHP.cfg.subtitle}</br>С нашим сервисом вы экономите на услугах до 50%.<br>
            Как это возможно?</h4>
        </div>
<!-- блок лендинга степов (1) --> 
		<div class="uk-grid" data-uk-grid-margin> 
            <div class="uk-width-medium-1-2">
				<img src="themes/{PHP.theme}/img/land/hiw-1.png" alt="">
            </div>
			<div class="uk-width-medium-1-2">
				<h3 class="uk-h1 tp-text-transform uk-margin-large-top uk-hidden-small"><i class="uk-icon-plus-circle"></i>&nbsp; 1. Создайте задание</h3> 
				<h3 class="tp-text-warning tp-text-transform uk-margin-large-top uk-visible-small"><i class="uk-icon-plus-circle"></i>&nbsp; 1. Создайте задание</h3> 
				<h3 class="uk-h3">Выберите категорию.</br>Опишите своими словами задачу, которую требуется выполнить. <br></h3>
			<!-- IF {PHP.usr.id} == 0 -->
				<a class="tp-button-warning tp-text-transform uk-button uk-button-large" data-uk-modal="" href="#log"><i class="uk-icon-plus-circle"></i>&nbsp; {PHP.L.projects_add_to_catalog}</a>
			<!-- ENDIF -->
			<!-- IF {PHP.usr.id} > 0 -->
				<a class="tp-button-warning tp-text-transform uk-button uk-button-large" href="{PHP|cot_url('projects', 'm=add')}"><i class="uk-icon-plus-circle"></i>&nbsp; {PHP.L.projects_add_to_catalog}</a>
			<!-- ENDIF -->
			</div>
		</div>
<!-- КОНЕЦ блок лендинга степов (1) -->
<!-- блок лендинга степов (2) -->
		<div class="uk-grid" data-uk-grid-margin> 

			<div class="uk-width-medium-1-2">
				<h3 class="uk-h1 tp-text-transform uk-margin-large-top uk-hidden-small"><i class="uk-icon-hand-o-right"></i>&nbsp; 2. Исполнители предложат вам свои услуги и цены</h3>
				<h3 class="tp-text-warning tp-text-transform uk-margin-large-top uk-visible-small"><i class="uk-icon-plus-circle"></i>&nbsp; 2. Исполнители предложат вам свои услуги и цены</h3> 				
				<h3 class="uk-h3">Уже скоро вы начнете получать предложения от исполнителей, готовых выполнить ваше задание. </br>! или же Вы можете прямо сейчас</h3>
				<a class="uk-button uk-button-large uk-button-success" href="{PHP.cot_groups.4.alias|cot_url('users', 'group='$this)}">ВЫБРАТЬ ИСПОЛНИТЕЛЯ</a>
			</div>
            <div class="uk-width-medium-1-2">
				<img src="themes/{PHP.theme}/img/land/hiw-2.png" alt="">
            </div>
		</div>
<!-- КОНЕЦ блок лендинга степов (1) -->		
<!-- блок лендинга степов (3) -->
		<div class="uk-grid" data-uk-grid-margin> 
            <div class="uk-width-medium-1-2">
				<img src="themes/{PHP.theme}/img/land/choose-expert1.png" alt="">
            </div>
			<div class="uk-width-medium-1-2">
				<h3 class="uk-h1 tp-text-transform uk-margin-large-top uk-hidden-small"><i class="uk-icon-plus-circle"></i>&nbsp; 3. Выберите лучшее предложение</h3>
				<h3 class="tp-text-warning tp-text-transform uk-margin-large-top uk-visible-small"><i class="uk-icon-plus-circle"></i>&nbsp; 3. Выберите лучшее предложение</h3>
				<h3 class="uk-h3">Вы сможете выбрать подходящего исполнителя, по разным критериям:</h3>
					<ul class="uk-list uk-list-line">
						<li><i class="uk-icon-money uk-margin-right"></i>Стоимость услуг</li>
						<li><i class="uk-icon-hand-peace-o uk-margin-right"></i>Примеры работ</li>
						<li><i class="uk-icon-star uk-margin-right"></i>Рейтинг</li>
						<li><i class="uk-icon-line-chart uk-margin-right"></i>Отзывы заказчиков</li>
					</ul>
			</div>
		</div>
<!-- КОНЕЦ блок лендинга степов (3) --> 
	</div> 
</div>
<!-- КОНЕЦ блок лендинга степов (1-2-3) -->
<div class="uk-block"style="background-color: #deeafb">
	<div class="uk-container uk-container-center uk-margin-top">
		<hr>
		<h1 class="uk-h1 uk-text-bold uk-text-center uk-margin-top uk-hidden-small">Основные преимущества сервиса <span class="tp-text-transform tp-text-jura tp-text-warning uk-text-bold">«{PHP.cfg.maintitle}»</span></h1>
		<h3 class="uk-text-bold uk-text-center uk-margin-top uk-visible-small">Основные преимущества сервиса</br> <span class="uk-h2 tp-text-transform tp-text-jura tp-text-warning uk-text-bold">«{PHP.cfg.maintitle}»</span></h3>
		<div class="uk-container uk-container-center">
		<h1 class="uk-h1 uk-text-bold tp-text-transform">
		<i class="uk-icon-money uk-icon-large tp-text-warning"></i> выгодные цены
		</h1>
			<h3 class="uk-h3 uk-text-bold">У частных исполнителей нет расходов на офис, рекламу, зарплату секретарю и других затрат, которые сервисные компании обычно включают в стоимость своих услуг. 
			</h3>
		</div>
		<div class="uk-container uk-container-center uk-margin-small">
		<h1 class="uk-h1 uk-text-bold tp-text-transform">
		<i class="uk-icon-check-square-o uk-icon-large tp-text-warning"></i> Проверенные исполнители</h1>
			<h3 class="uk-h3 uk-text-bold">Все исполнители сервиса «{PHP.cfg.maintitle}» проходят процедуру верификации, мы проверяем отзывы, разбираемся с жалобами и контролируем добросовестность их работы на основе отзывов
			</h3>
		</div>		
		<div class="uk-container uk-container-center uk-margin-small">
		<h1 class="uk-h1 uk-text-bold tp-text-transform">
		<i class="uk-icon-clock-o uk-icon-large tp-text-warning"></i> Экономия времени</h1>
			<h3 class="uk-h3 uk-text-bold">На сервисе «{PHP.cfg.maintitle}» вы можете найти подходящего исполнителя за несколько минут. Многие из них готовы приступить к работе в тот же день, а иногда в тот же час.
			</h3>	
		</div>
	</div>
		<hr>
</div> 
<div class="uk-block uk-block-muted">
	<div class="uk-container uk-container-center  uk-margin-large-top"> 
         <div class="tp-paddin-title">
            <h2 class="uk-h1 uk-text-center uk-text-break">Задания, которые на <span class="tp-text-transform tp-text-jura tp-text-warning uk-text-bold">«{PHP.cfg.maintitle}»</span> выполняются прямо сейчас</h2>
        </div>
        <!-- IF {PHP.cot_plugins_active.paypro} -->
            <!-- IF !{PHP|cot_getuserpro()} AND {PHP.cfg.plugin.paypro.projectslimit} > 0 AND {PHP.cfg.plugin.paypro.projectslimit} <= {PHP.usr.id|cot_getcountprjofuser($this)} -->
            <div class="uk-alert uk-alert-warning">{PHP.L.paypro_warning_projectslimit_empty}</div>
            <!-- ENDIF -->
            <!-- IF !{PHP|cot_getuserpro()} AND {PHP.cfg.plugin.paypro.offerslimit} > 0 AND {PHP.cfg.plugin.paypro.offerslimit} <= {PHP.usr.id|cot_getcountoffersofuser($this)} -->
            <div class="uk-alert uk-alert-warning">{PHP.L.paypro_warning_offerslimit_empty}</div>
            <!-- ENDIF -->
        <!-- ENDIF -->
        {PROJECTS_SEARCH}
		<div class="uk-grid" data-uk-grid-margin=""> 
			<div class="uk-width-medium-7-10">

			{PROJECTS}
				<div class="uk-container uk-container-center uk-margin-bottom uk-text-center">		
					<a class="uk-button uk-button-large uk-button-primary uk-text-bold" href="projects"> ПОСМОТРЕТЬ ВСЕ ЗАДАНИЯ</a>
				</div> 
			</div>
					  
			<div class="uk-width-medium-3-10">
<div class="uk-text-center">
			<div class="uk-panel uk-panel-box uk-width-medium-1-1 uk-panel-box-hover uk-text-middle">
				<figure class="uk-overlay uk-overlay-hover">
					<a href="#link">
					<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/land/master-stat-01.png" href="#link" width="274" height="305" alt="" />
					<div class="uk-overlay-panel uk-ignore uk-flex uk-flex-center uk-flex-middle uk-text-center uk-overlay-background uk-overlay-spin"><h3 class="uk-text-contrast uk-h2 uk-text-bold"><a href="#link">Как стать исполнителем?</h3></div>
					</a>
				</figure>
			</div>

			<div class="uk-panel uk-panel-box uk-width-medium-1-1 uk-panel-box-hover uk-text-middle">
				<figure class="uk-overlay uk-overlay-hover">
					<a href="#link">
					<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/land/master-stat-02.png" href="#link" width="274" height="305" alt="" />
					<div class="uk-overlay-panel uk-ignore uk-flex uk-flex-center uk-flex-middle uk-text-center uk-overlay-background uk-overlay-spin"><h3 class="uk-text-contrast uk-h2"><a href="#link">Безопасность и гарантии</h3></div>
					</a>
				</figure>
			</div>

			<div class="uk-panel uk-panel-box uk-width-medium-1-1 uk-panel-box-hover uk-text-middle">
				<figure class="uk-overlay uk-overlay-hover">
					<a href="#link">
					<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/land/master-stat-03.png" href="#link" width="274" height="305" alt="" />
					<div class="uk-overlay-panel uk-ignore uk-flex uk-flex-center uk-flex-middle uk-text-center uk-overlay-background uk-overlay-spin"><h3 class="uk-text-bold uk-text-contrast tp-text-jura uk-h2"><a href="#link">Самые необычные задания</h3></div>
					</a>
				</figure>
			</div>
			</div></div>				
		</div>			
	</div>
</div>
<!-- IF {PHP.cot_plugins_active.userpoints} -->

<div class="uk-block uk-block-muted" id="top-user">
    <div class=" uk-margin-large-top uk-text-center uk-width-1-2 uk-container-center uk-margin-bottom">
		<h2 class="uk-h1 tp-text-transform uk-text-success uk-margin-remove">{PHP.L.Top_performers_title}</h2>
		<h3 class="tp-text-transform uk-margin-remove uk-text-bold">{PHP.L.Top_performers_description}</h3>
        <i class="uk-icon-star-o uk-text-large tp-color-ts uk-margin-top"></i>

    </div>
    
    <div class="uk-container uk-container-center uk-margin-large-top">
        <div class="uk-grid-width-small-1-1 uk-grid-width-medium-1-2" data-uk-grid="{gutter: 30}">
            {PHP|cot_get_topusers (4, 8)}
        </div>
    </div>
</div>

<!-- ENDIF -->
<!-- IF {PHP.cot_plugins_active.paytop} -->
<div class="uk-block uk-block-muted uk-margin-remove" id="index-paytop">
    <div class="uk-text-center uk-width-1-2 uk-container-center uk-margin-bottom">
        <h4 class="uk-h6 uk-text-muted uk-margin-remove tp-text-transform"><!-- IF {PHP.usr.isadmin} OR {PHP.usr.maingrp} == 4 OR {PHP.usr.maingrp} == 8 -->{PHP.L.Advertising_space_title}<!-- ELSE -->Рекомендуем<!-- ENDIF --></h4>
        <h3 class="uk-margin-remove uk-text-bold tp-text-transform"><!-- IF {PHP.usr.isadmin} OR {PHP.usr.maingrp} == 4 OR {PHP.usr.maingrp} == 8 -->{PHP.L.Advertising_space_description}<!-- ELSE -->Популярные пользователи<!-- ENDIF --></h3>
        <i class="uk-icon-minus tp-color-ts"></i>
    </div>
    {PHP|cot_get_paytop ('top')}
</div>
<!-- ENDIF -->



<!-- РАЗДЕЛ ПОРТФОЛИО -->    
<!-- IF {PHP.cot_modules.folio} -->
<div class="uk-block">
    <div class="uk-text-center uk-width-1-2 uk-container-center uk-margin-bottom">
		<h2 class="uk-h1 tp-text-transform uk-text-primary uk-margin-remove">{PHP.L.folio}</h2>
		<h3 class="tp-text-transform uk-margin-remove uk-text-bold">{PHP.L.Folio_description}</h3>
        <i class="uk-icon-star-o uk-text-large tp-color-ts uk-margin-top"></i>
    </div>
    <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-4" data-uk-grid="">
        {PHP|cot_getfoliolist('index', '8')}
    </div>
</div>    
<!-- ENDIF -->
<!-- РАЗДЕЛ ПОРТФОЛИО КОНЕЦ--> 
<!-- IF {PHP.cot_modules.market} -->
<div class="uk-block uk-block-muted">
<div class="uk-container uk-container-center uk-margin-large-top">
            <div class="uk-text-center">

		<h2 class="uk-h1 tp-text-transform uk-text-success uk-margin-remove">{PHP.L.market}</h2>
		<h3 class="tp-text-transform uk-margin-remove uk-text-bold">{PHP.L.Market_description_index}</h3>
        <i class="uk-icon-star-o uk-text-large tp-color-ts uk-margin-top"></i>
                <hr class="uk-divider-icon uk-width-1-2" style="margin: auto;"> 
            </div>
    <div>
        {PHP|cot_getmarketlist('index', '3')}
    </div>
</div>
</div>
<!-- ENDIF -->
<!-- IF {PHP.cot_plugins_active.indexnews} -->
<div class="uk-block">
	<div class="uk-text-center uk-width-1-2 uk-container-center uk-margin-bottom">
        <i class="uk-icon-newspaper-o uk-h2 tp-color-ts uk-margin-bottom"></i>
		<h2 class="uk-h1 tp-text-transform uk-text-success uk-margin-remove">{PHP.L.News_title}</h2>
		<h3 class="tp-text-transform uk-margin-remove uk-text-bold">{PHP.L.News_description}</h3>
    </div>


<div class="uk-container uk-container-center uk-margin-large-bottom">
    <div class="uk-grid" data-uk-grid-margin="">
        {INDEX_NEWS}
    </div>
</div>
</div>
<!-- ENDIF -->
<!-- IF {PHP.cot_plugins_active.primebox} -->
<div class="uk-block uk-block-muted" id="index-primebox">
    <div class="uk-text-center uk-width-1-2 uk-container-center uk-margin-bottom">
        <i class="uk-icon-comments uk-h2 tp-color-ts uk-margin-bottom"></i>
		<h2 class="uk-h1 tp-text-transform uk-text-success uk-margin-remove">Prime Box</h2>
		<h3 class="tp-text-transform uk-margin-remove uk-text-bold">Место для вашей рекламы</h3>
    </div>
    <div class="uk-margin-large-top">
            {PHP|primebox_show('index', 8)}
    </div>
</div>
<!-- ENDIF -->

<!-- IF {PHP.cot_plugins_active.ads} -->
<div class="uk-block uk-block-muted uk-hidden-small">
    <div class="uk-text-center uk-width-1-2 uk-container-center uk-margin-bottom">
        <i class="uk-icon-comments uk-h2 tp-color-ts uk-margin-bottom"></i>
		<h2 class="uk-h1 tp-text-transform uk-text-success uk-margin-remove">РЕКЛАМА</h2>
		<h3 class="tp-text-transform uk-margin-remove uk-text-bold">баннеры наших рекламодателей</h3>
    </div>
    
    <div class="uk-container uk-container-center uk-margin-large-top">
        <div class="uk-grid-width-small-1-1 uk-grid-width-medium-1-2" data-uk-grid="{gutter: 30}">
            {PHP|ads_show('index', 2)}
        </div>
    </div>
</div>
<!-- ENDIF -->




<!-- END: MAIN -->
