<?php
/**
 * Пользовательский файл локализации шаблона
 * @Site https://mydomain.com/
 * @package Cotonti
 * @version 0.9.19 FL 2.7.0
 * @author WEBITPROFF
 * @copyright Copyright (c) 2018 powered by WEBITPROFF
 * @license https://mydomain.com/license
 */

defined('COT_CODE') or die('Wrong URL.');

/**
 * Данный файл предназначен для дополнительных пользовательских локализационных строк
 */

#BLOCK SOCIAL NETWORKS
$L['Twitter_url'] = '';
$L['Vk_url'] = '';
$L['Google_plus_url'] = '';
$L['Facebook_url'] = '';
$L['Youtube_url'] = '';
#END BLOCK SOCIAL NETWORKS
#BLOCK index to employer
$L['title_employer_index_block'] = 'Заказчикам';
$L['1st_title_employer_index_block'] = 'Разместить задание';
$L['1st_descript_employer_index_block'] = 'Максимально подробно распишите свой вопрос. Укажите максимум информации о продукции. Обязательно прикрепите к запросу файл с техническим описанием.';
$L['2nd_title_employer_index_block'] = 'Получите предложения от исполнителей';
$L['2nd_descript_employer_index_block'] = 'Уже в течение дня исполнители предложат варианты оформления документов.';
$L['3rd_title_employer_index_block'] = 'Выберите лучшее предложение';
$L['3rd_descript_employer_index_block'] = 'Выберите исполнителя с лучшим предложением.';
#END BLOCK index to employer

#BLOCK index to freelancer
$L['title_freelancer_index_block'] = 'Исполнителям и продавцам услуг';
$L['1st_title_freelancer_index_block'] = 'Добавьте свои услуги';
$L['1st_descript_freelancer_index_block'] = 'Разместите услугу или несколько услуг которые оказывает ваша компания.';
$L['2nd_title_freelancer_index_block'] = 'Получите заказ';
$L['2nd_descript_freelancer_index_block'] = 'Ваши услуги в каталоге, умения, рейтинг, - залог быстрого получения заказа на ваши услуги!';
$L['3rd_title_freelancer_index_block'] = 'Выполните заказ';
$L['3rd_descript_freelancer_index_block'] = 'Соблюдайте условия и делайте качественно. Не стесняйтесь просить положительный отзыв о выполненной работе.';
$L['4th_title_freelancer_index_block'] = 'Получите оплату';
$L['4th_descript_freelancer_index_block'] = 'А так же хороший рейтинг и ещё больше заказов. Наш сервис ценит и поощряет добросовестных исполнителей.';
#END BLOCK index to freelancer
#BLOCK HEADER меню 
$L['groups_4_descript'] = 'Анкеты частных исполнителей и мастеров';
$L['groups_8_descript'] = 'Каталог фирм и предпринимателей, профессионально оказывающих услуги';
$L['groups_7_descript'] = 'Каталог заказчиков и работодателей';
$L['market_menu_descript'] = 'Витрина объявлений и предложений своих услуг от исполнителей';
$L['projects_menu_descript'] = 'Раздел объявлений о заказах, на выполнение бытовых и коммерческих услуг';
$L['folio_menu_descript'] = 'Раздел портфолио исполнителей, выполненных ими работ и достижений';
#END BLOCK HEADER меню

$L['Register'] = 'Регистрация';
$L['users_lostpass'] = 'пароль утерян?';
$L['Log'] = 'Логин/Никнейм';
$L['Send_inquiry'] = 'Отправить запрос';
$L['All_posts_pmlist'] = 'Все сообщения';
$L['Contacty_title'] = 'Контакты';


$L['I_am_an_freelancer'] = 'Я Частный Специалист';
$L['reg_as_freelancer'] = 'Зарегистрироваться как - Частный Специалист';
$L['I_am_an_company'] = 'Я Компания или ФЛП';
$L['reg_as_company'] = 'Зарегистрироваться как - Компания или ФЛП';
$L['I_am_an_employer'] = 'Я Заказчик';
$L['reg_as_employer'] = 'Зарегистрироваться как - Заказчик';

#BLOCK 1
$L['Turnkey_solution_title'] = 'Безопасная работа';
$L['Turnkey_solution_description'] = 'Резервирование средств на сервисе позволит вам быть уверенными в честном и успешном исходе проекта.';
#END BLOCK 1

#BLOCK 2
$L['Adaptive_pattern_title'] = 'Адаптивный шаблон';
$L['Adaptive_pattern_description'] = 'Адаптивный шаблон под мобильные<br /> устройства на cotonti.';

$L['Active_performers_description_2'] = 'Отзывы других заказчиков о выполненных проектах, проверенные личные данные фрилансеров.';
#END BLOCK 2

#BLOCK 3
$L['Design_title'] = 'Мощная платформа';
$L['Design_description'] = 'В вашем распоряжении удобная система личных сообщений, форум проекта, вложения и другие инструменты. ';
#END BLOCK 3

#INFO BLOCK THEMES IMG
$L['Welcome_to_freelance_title'] = 'Добро пожаловать на';
$L['Welcome_to_freelance_description'] = 'Получай заказы с гарантией оплаты от лучших заказчиков рунета и<br /> зарабатывай на постоянном потоке заказов';
#END INFO BLOCK THEMES IMG

$L['Ak_pro'] = 'PRO';
$L['Users_signature_description'] = 'Подпись пользователя - заполняется в профиле пользователя';
$L['Create_an_account_title'] = 'Создать аккаунт';
$L['Coment_title'] = 'Комментарии';
$L['Sellers_other_items_title'] = 'Другие товары продавца';
$L['Similar_items_title'] = 'Пхожие товары';
$L['Build_timeago_title'] = 'Был(а) на сайте:';
$L['All_title'] = 'Для всех';


#REGISTER FORM INFO
//Работодатель form register = /register
$L['Employer_description'] = '<strong>Работодатель</strong> — физическое или юридическое лицо, поручающее выполнение конкретной работы или задачи, не прибегая к официальному трудоустройству исполнителя.';
//Фрилансер
$L['Freelancer_description'] = '<strong>Частный мастер или исполнитель поручений</strong>, который с целью подработки и/или постоянного дохода, выполняет разовые и переодические задания от разных работодателей.';
//Фрилансер
$L['Freelancerul_description'] = '';
$L['Company_description'] = '<strong>Компания или предприниматель</strong>, имеющие должную квалификацию и опыт, для  оказывания профиссиональных услуг: репетитор, юрист, автоинструктор и т. п.';
#END REGISTER FORM


#FORM REGISTER
$L['Log_info_text'] = 'Логин: До 15 символов. Только латинские символы. <span class="uk-text-bold uk-text-danger">*</span>';
$L['Pasrec_youremail_info_text'] = 'Действующий e-mail: (необходим для подтверждения регистрации) <span class="uk-text-bold uk-text-danger">*</span>';
$L['Password_info_text'] = 'Пароль: От 5 до 30 любых символов <span class="uk-text-bold uk-text-danger">*</span>';
$L['Password_info_text2'] = 'Подтвердить пароль. <span class="uk-text-bold uk-text-danger">*</span>';
$L['Useragreement_agree'] = 'Я ознакомлен с "Пользовательским соглашением"';
$L['Captcha'] = 'Введите ответ';
$L['Captcha_title'] = 'Капча: сколько будет';
# END FORM REGISTER

$L['My_profil'] = 'Профиль';
$L['Edit_own_profile'] = 'Править свой профиль';
$L['Administrator_title'] = 'Aдминистратор';
$L['My_profil_signature'] = 'Ваша анкета — это ваша визитная карточка. Опишите ваши сильные стороны, умения и таланты, например укажите: <br />основные услуги, которые вы предоставляете; <br />наличие профессионального инструмента (если вы мастер);<br /> программы, языки программирования и т. д., с которыми работаете (если вы фрилансер);<br /> наличие автомобиля, диплома, допуска и т. д. - укажите специфическую информацию, которая присуща вашей специализации; <br />ваши сильные качества, которые помогают вам в работе; <br />свои интересные хобби - это сделает вашу анкету более живой.';

$L['Market_freelancing_title'] = 'биржа уникального контента';
$L['Market_remote_work_title'] = 'Уникальный контент на продажу и под заказ';

$L['Market_remote_work_description'] = 'Биржа копирайтинга предлагает услуги по написанию информационных статей, SEO-текстов, новостей, рефератов, курсовых, дипломных работ, услуги переводчиков, корректоров!';

#BLOCK FOLIO
$L['Folio_title'] = 'Портфолио';
$L['Folio_description'] = 'Новые работы в разделе';
#END BLOCK FOLIO

#BLOCK REVIEWS
$L['index_reviews'] = 'Последние отзывы';
$L['index_reviews_description'] = 'Отзывы пользователей о работе на бирже услуг';
#END BLOCK REVIEWS

#BLOCK PROJECTS
$L['Projects_title'] = 'Проекты';
$L['Projects_description'] = 'Новые задания исполнителям';
$L['Projects_catdescription'] = 'Задания по категориям';
$L['Ifo_rpo_title'] = 'Пожалуйста выберите какой будет проект';

#END BLOCK PROJECTS

#BLOCK NEWS
$L['News_title'] = 'Новости';
$L['News_description'] = 'Последние посты';
$L['Cat_title'] = 'Категории';
#END BLOCK NEWS

#BLOCK USER
$L['Top_performers_title'] = 'Проверенные исполнители';
$L['Top_performers_description'] = 'Лучшие исполнители';
$L['Account_title'] = 'Аккаунт';
$L['Rating_title'] = 'Рейтинг';
$L['Skills_title'] = 'Навыки';
$L['To_send_a_letter'] = 'Отправить письмо';
#END BLOCK USER

#BLOCK PAGE
$L['Pages_title'] = 'Новости сайта';
$L['Pages_description'] = 'Самые свежие новости сайта freelance';

#Unvalidated
$L['Unvalidated_pages_description'] = 'Ваши статьи которые находятся на утверждении';
$L['Top_news_title'] = 'Тор 10 новостей';
$L['Twitter_title'] = 'Twitter';

#footer enum
$L['Enum_pages_title'] = 'Последние новости';
#END BLOCK PAGE

#BLOCK PROJECTS
//выводиться только по ссылке  /projects
$L['Projects_description_list'] = '';
$L['Proposals_title'] = 'Предложений';
$L['Views_title'] = 'Просмотров';
$L['Region_title'] = 'Регион';
$L['In_execution'] = 'На исполнение';
$L['Opened'] = 'Открыт';
$L['Simprojects_title'] = 'Похожие проекты';
#END BLOCK PROJECTS

#BLOCK MARKET
$L['Market_description_list'] = '';
$L['Market_description_index'] = 'Новые предложения своих услуг от исполнителей';
$L['Enum_pages_market_title'] = 'Последние новости на сайте';
#END BLOCK MARKET

#BLOCK FOLIO
$L['Folio_description_list'] = '';
$L['All_works_title'] = 'Все работы';
$L['All_works_market_title'] = 'Все товары';
$L['Requirements'] = 'Требования';
#END BLOCK FOLIO

#ADVERTISING SPACE - plugs userspoins
$L['Advertising_space_title'] = 'Как сделать рекламу?';
$L['Advertising_space_description'] = 'Место для рекламы';
$L['Advertising_space_description_lincks'] = '<b>Реклама</b> – это процесс распространения информации о чем-либо с целью получения положительного оклика от целевой аудитории.';
#END ADVERTISING SPACE

# FOOTER

// Популярные категории
$L['Popular_categories_title'] = 'Популярные категории';

$L['Popular_categories_url1'] = 'Программирование';
$L['Popular_categories_url2'] = 'Менеджмент';
$L['Popular_categories_url3'] = 'Маркетинг и реклама';
$L['Popular_categories_url4'] = 'Дизайн';
$L['Popular_categories_url5'] = 'Оптимизация (SEO)';
$L['Popular_categories_url6'] = 'Тексты';

//Полезные ссылки
$L['Useful_links_title'] = 'Сервисный раздел';
$L['Useful_links_url1'] = 'О нашем сервисе';
$L['Useful_links_url2'] = 'Как заказать услугу';
$L['Useful_links_url4'] = ' Как стать исполнителем';
$L['Useful_links_url5'] = 'Как зарегистрировать компанию';
$L['Useful_links_url6'] = 'Гарантия и безопасность';
$L['Useragreement_title'] = 'Пользовательское соглашение';
$L['publicoffert_title'] = 'Публичная оферта';

//PAYMENTS
$L['Payments_info_min_title'] = 'Минимальная сумма для перевода';
$L['Payments_info_max_title'] = 'Максимальная сумма для перевода';

$L['Payments_info_min_perevod_title'] = 'Минимальная сумма для вывода со счета';
$L['Payments_info_max_perevod_title'] = 'Максимальная сумма для вывода со счета';

$L['Attention_title'] = 'Внимания';

$L['Forder_title'] = 'Папки';
$L['PAss_title1'] = 'Пароль 1';
$L['PAss_title2'] = 'Еще раз';
$L['rewes_title'] = 'Оставить отзыв';

/* All */
$L['sbr_mydeals_tile_etb'] = 'Этапы сделки';
$L['page_file_add'] = 'Прикрепить файл';
$L[''] = '';
$L[''] = '';

#BLOCK FORUMS
$L['Search_the_forum'] = 'Поиск на форуме';
$L['Copirayter_tittle'] = '<a href="https://mydomen.com/">mydomen.com</a>';
$L[''] = '';
$L[''] = '';
#END FORUMS

#BLOCK USERS CUSTOM
$L['Edit_as_admin'] = 'Править как Админ';
$L['Edit_my_profile'] = 'Правка своего профиля';
$L['paytop_userpage'] = 'Рекламировать страницу';
#END USERS CUSTOM


/**
 * ! всё что ниже этой строки скопировать и вставить в одноимённый файл, - masterscity.ru.lang.php */

# START_ №1 BLOCK INDEX_PROJECTS_CATS
$L['Name_parent_category_1'] = 'Тех-Документация';
$L['Url_parent_category_1'] = 'index.php?e=projects&c=tekh-dokumentaciya';

$L['Name_sub_category_1_1'] = 'Разработка изделий';
$L['Url_sub_category_1_1'] = 'index.php?e=projects&c=razrabotka-izdelij';

$L['Name_sub_category_1_2'] = 'Оцифровка чертежей';
$L['Url_sub_category_1_2'] = 'index.php?e=projects&c=ocifrovka-chertezhej';

$L['Name_sub_category_1_3'] = 'Проектирование';
$L['Url_sub_category_1_3'] = 'index.php?e=projects&c=proektirovanie';

$L['Name_sub_category_1_4'] = 'Тех экспертиза';
$L['Url_sub_category_1_4'] = 'index.php?e=projects&c=tekh-ehkspertiza';

$L['See_All'] = 'Смотреть все';
# END _ №1 BLOCK INDEX_PROJECTS_CATS


# START_ №2 BLOCK INDEX_PROJECTS_CATS
$L['Name_parent_category_2'] = 'Монтаж и сборка';
$L['Url_parent_category_2'] = 'index.php?e=projects&c=montazh-i-sborka';

$L['Name_sub_category_2_1'] = 'Монтаж детских площадок';
$L['Url_sub_category_2_1'] = 'index.php?e=projects&c=montazh-dp';

$L['Name_sub_category_2_2'] = 'Сборка мебели';
$L['Url_sub_category_2_2'] = 'index.php?e=projects&c=sborka-mebeli';

$L['Name_sub_category_2_3'] = 'Ремонт и обслуживание';
$L['Url_sub_category_2_3'] = 'index.php?e=projects&c=remont-i-obsluzhivanie';

$L['Name_sub_category_2_4'] = 'Благоустройство';
$L['Url_sub_category_2_4'] = 'index.php?e=projects&c=blagoustrojstvo';

$L['See_All'] = 'Смотреть все';
# END _ №2 BLOCK INDEX_PROJECTS_CATS

# START_ №3 BLOCK INDEX_PROJECTS_CATS
$L['Name_parent_category_3'] = 'Маркетинг и реклама';
$L['Url_parent_category_3'] = 'index.php?e=projects&c=marketing';

$L['Name_sub_category_3_1'] = 'Социальные сети';
$L['Url_sub_category_3_1'] = 'index.php?e=projects&c=socialnye-seti';

$L['Name_sub_category_3_2'] = 'Контекстная реклама';
$L['Url_sub_category_3_2'] = 'index.php?e=projects&c=kontekstnaya-reklama';

$L['Name_sub_category_3_3'] = 'E-mail маркетинг';
$L['Url_sub_category_3_3'] = 'index.php?e=projects&c=e-mail-marketing';

$L['Name_sub_category_3_4'] = 'Базы данных клиентов';
$L['Url_sub_category_3_4'] = 'index.php?e=projects&c=bazy-dannyh-klientov';

$L['See_All'] = 'Смотреть все';
# END _ №3 BLOCK INDEX_PROJECTS_CATS

# START_ №4 BLOCK INDEX_PROJECTS_CATS
$L['Name_parent_category_4'] = 'Перевозчики и курьеры';
$L['Url_parent_category_4'] = 'index.php?e=projects&c=perevozchiki-i-kurery';

$L['Name_sub_category_4_1'] = 'Грузоперевозки по России';
$L['Url_sub_category_4_1'] = 'index.php?e=projects&c=gruzoperevozki-po-rossii';

$L['Name_sub_category_4_2'] = 'Грузоперевозки местные';
$L['Url_sub_category_4_2'] = 'index.php?e=projects&c=gruzoperevozki-mestnye';

$L['Name_sub_category_4_3'] = 'Экспедиторы';
$L['Url_sub_category_4_3'] = 'index.php?e=projects&c=ehkspeditory';

$L['Name_sub_category_4_4'] = 'Курьерские услуги';
$L['Url_sub_category_4_4'] = 'index.php?e=projects&c=kurerskie-uslugi';

$L['See_All'] = 'Смотреть все';
# END _ №4 BLOCK INDEX_PROJECTS_CATS

# START_ №5 BLOCK INDEX_PROJECTS_CATS
$L['Name_parent_category_5'] = '3D модели и дизайн';
$L['Url_parent_category_5'] = 'index.php?e=projects&c=seo';

$L['Name_sub_category_5_1'] = '3D Проектирование';
$L['Url_sub_category_5_1'] = 'index.php?e=projects&c=3d-proektirovanie';

$L['Name_sub_category_5_2'] = 'Дизайнерские услуги';
$L['Url_sub_category_5_2'] = 'index.php?e=projects&c=dizajnerskie-uslugi';

$L['Name_sub_category_5_3'] = 'Графика и рисунок';
$L['Url_sub_category_5_3'] = 'index.php?e=projects&c=grafika-i-risunok';

$L['Name_sub_category_5_4'] = 'Логотипы';
$L['Url_sub_category_5_4'] = 'index.php?e=projects&c=logotipy';

$L['See_All'] = 'Смотреть все';
# END _ №5 BLOCK INDEX_PROJECTS_CATS

# START_ №6 BLOCK INDEX_PROJECTS_CATS
$L['Name_parent_category_6'] = 'Веб-программирование';
$L['Url_parent_category_6'] = 'index.php?e=projects&c=web-programmirovanie';

$L['Name_sub_category_6_1'] = 'Оптимизация (SEO)';
$L['Url_sub_category_6_1'] = 'index.php?e=projects&c=optimizaciya-seo';

$L['Name_sub_category_6_2'] = 'Доработка веб-сайтов';
$L['Url_sub_category_6_2'] = 'index.php?e=projects&c=dorabotka-sajtov';

$L['Name_sub_category_6_3'] = 'Создание веб-сайтов';
$L['Url_sub_category_6_3'] = 'index.php?e=projects&c=sozdanie-sajtov';

$L['Name_sub_category_6_4'] = 'Создание приложений';
$L['Url_sub_category_6_4'] = 'index.php?e=projects&c=app-making';

$L['See_All'] = 'Смотреть все';
# END _ №6 BLOCK INDEX_PROJECTS_CATS

# START_ №7 BLOCK INDEX_PROJECTS_CATS
$L['Name_parent_category_7'] = 'Производство';
$L['Url_parent_category_7'] = 'index.php?e=projects&c=proizvodstvo';

$L['Name_sub_category_7_1'] = 'Деревообработка';
$L['Url_sub_category_7_1'] = 'index.php?e=projects&c=derevoobrabotka';

$L['Name_sub_category_7_2'] = 'Металлообработка';
$L['Url_sub_category_7_2'] = 'index.php?e=projects&c=metalloobrabotka';

$L['Name_sub_category_7_3'] = 'Окрасочные работы';
$L['Url_sub_category_7_3'] = 'index.php?e=projects&c=okrasochnye-raboty';

$L['Name_sub_category_7_4'] = 'Материалы и комплектующие';
$L['Url_sub_category_7_4'] = 'index.php?e=projects&c=materialy-i-komplektuyushchie';
$L['See_All'] = 'Смотреть все';
# END _ №7 BLOCK INDEX_PROJECTS_CATS

# START_ №8 BLOCK INDEX_PROJECTS_CATS
$L['Name_parent_category_8'] = 'Менеджмент';
$L['Url_parent_category_8'] = 'index.php?e=projects&c=menedzhment';

$L['Name_sub_category_8_1'] = 'Налоговая и бухучет';
$L['Url_sub_category_8_1'] = 'index.php?e=projects&c=nalogovaya-i-buhuchet';

$L['Name_sub_category_8_2'] = 'Юридические услуги';
$L['Url_sub_category_8_2'] = 'index.php?e=projects&c=yuridicheskie-uslugi';

$L['Name_sub_category_8_3'] = 'Персональный помощник';
$L['Url_sub_category_8_3'] = 'index.php?e=projects&c=personalnyj-pomoshchnik';

$L['Name_sub_category_8_4'] = 'Менеджер проектов';
$L['Url_sub_category_8_4'] = 'index.php?e=projects&c=menedzher-proektov';

$L['See_All'] = 'Смотреть все';
# END _ №8 BLOCK INDEX_PROJECTS_CATS


