<!-- BEGIN: MAIN -->
<div class="uk-block" style="position: relative; background-image: url('themes/{PHP.theme}/img/tp-header-top.png'); background-attachment: fixed;">
    <div class="uk-container uk-container-center"> 
        <h1 class="uk-text-bold uk-text-contrast">{PHP.L.affiliate}</h1>
    </div>
</div>
<div class="uk-container uk-container-center uk-margin-top">
    <ul class="uk-breadcrumb">
        <li><a href="{PHP.cfg.mainurl}">{PHP.L.Home}</a></li>
        <li class="uk-active"><span>{PHP.L.affiliate}</span></li>
    </ul>
</div>
<div class="uk-block uk-block-muted">
    <div class="uk-container uk-container-center uk-margin-large-top">
        <div class="tp-box">
            <div class="tp-box-header tp-with-border">

				<h4>{PHP.L.affiliate_link_title}:</h4>
				<p>{PHP.cfg.mainurl}/?ref={PHP.usr.id}</p>
				<br/>
				<br/>
				<h4>{PHP.L.affiliate_tree_title}:</h4>
				{REFERALS_TREE}

	<!-- BEGIN: PAYMENTS -->
		<br/>
		<br/>
		<h3 class="uk-article-subtitle">{PHP.L.affiliate_payments_title}:</h3>

		<div class="uk-overflow-container">
			<table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
				<thead>
					<tr>
						<th>#</th>
						<th>{PHP.L.Date}</th>
						<th>{PHP.L.affiliate_referal}</th>
						<th>{PHP.L.payments_summ} {PHP.cfg.payments.valuta}</th>
					</tr>
				</thead>

				<tbody>
				<!-- BEGIN: PAY_ROW -->
					<tr>
						<td>{PAY_ROW_ID}</td>
						<td>{PAY_ROW_PDATE|cot_date('d.m.Y H:i', $this)}</td>
						<td>{PAY_ROW_REFERAL_NAME}</td>
						<td>{PAY_ROW_SUMM|number_format($this, '2', '.', ' ')}</td>
					</tr>
				<!-- END: PAY_ROW -->
				</tbody>
			</table>
		</div>

		<p><b>{PHP.L.payments_allpayments}: {PAYMENT_SUMM|number_format($this, '2', '.', ' ')} {PHP.cfg.payments.valuta}</b></p>	
		
	<!-- END: PAYMENTS -->
            </div>
        </div>
    </div>
</div>
<!-- END: MAIN -->