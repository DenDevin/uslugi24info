<!-- BEGIN: MAIN -->
<div class="uk-block uk-block-muted">
    <div class="uk-container uk-container-center">
		<div class="uk-panel uk-panel-box uk-margin-bottom">
			<div class="tp-panel-body">
			<div class="uk-text-center"><h1 class="uk-text-bold">{PHP.L.fkbilling_title}</h1></div>

		<!-- BEGIN: ERROR -->
			<h4>{FK_TITLE}</h4>
			<div class="uk-alert uk-alert-large">{FK_ERROR}</div>
			<div class="uk-margin-bottom uk-text-center">
			<!-- IF {PHP.usr.id} > 0 -->
				<a class="uk-button uk-button-success uk-button-large" href="{PHP.usr.name|cot_url('users', 'm=details&u='$this)}">Перейти на свою страницу</a>
			<!-- ENDIF -->
			</div>	
			
			<!-- IF {FK_REDIRECT_URL} -->
			<br/>
			<p class="small">{FK_REDIRECT_TEXT}</p>
			<script>
				$(function(){
					setTimeout(function () {
						location.href='{FK_REDIRECT_URL}';
					}, 5000);
				});
			</script>
			<!-- ENDIF -->
		<!-- END: ERROR -->
			</div> 
		</div>
    </div> 
</div>
<!-- END: MAIN -->