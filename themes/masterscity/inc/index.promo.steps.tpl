<div class="uk-container uk-container-center uk-text-center uk-margin-top uk-margin-bottom">
	<hr />
	<h2 class="tp-text-transform uk-text-success">{PHP.L.title_employer_index_block}</h2>
	<div class="uk-grid tm-grid-margin-large" data-uk-grid-match="{target:'.uk-panel'}" data-uk-grid-margin="">
		<div class="uk-width-medium-1-3 uk-row-first" data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:300, repeat: true}">
			<a style="min-height: 150px;" class="uk-panel uk-panel-box uk-panel-box-primary tm-panel-link" href="">
			<div>
				<img src="themes/{PHP.theme}/img/land/contest-1.png" height="" width="" alt="{PHP.cfg.maintitle}">
			</div>
				<h2>{PHP.L.1st_title_employer_index_block}</h2>
				<p>{PHP.L.1st_descript_employer_index_block}</p>
			</a>
		</div>
		<div class="uk-width-medium-1-3" data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:600, repeat: true}">
			<a style="min-height: 150px;" class="uk-panel uk-panel-box uk-panel-box-primary uk-panel-box-primary-hover uk-panel-link" href="">
				<div>
					<img src="themes/{PHP.theme}/img/land/contest-2.png" height="" width="" alt="{PHP.cfg.maintitle}">
				</div>
				<h2>{PHP.L.2nd_title_employer_index_block}</h2>
				<p>{PHP.L.2nd_descript_employer_index_block}</p>
			</a>
		</div>
		<div class="uk-width-medium-1-3" data-uk-scrollspy="{cls:'uk-animation-slide-right', delay:900, repeat: true}">
			<a style="min-height: 150px;" class="uk-panel uk-panel-box uk-panel-box-primary uk-panel-link" href="">
				<div>
					<img src="themes/{PHP.theme}/img/land/contest-3.png" height="" width="" alt="{PHP.cfg.maintitle}">
				</div>
				<h2>{PHP.L.3rd_title_employer_index_block}</h2>
				<p>{PHP.L.3rd_descript_employer_index_block}</p>
			</a>
		</div>
	</div>
	<!-- IF {PHP.usr.id} == 0 -->
	<div class="uk-margin-top uk-text-center">
		<a class="uk-button uk-button-warning uk-button-large" data-uk-modal="" href="#log">{PHP.L.projects_add_to_catalog}</a>
	</div>
	<!-- ENDIF -->
	<!-- IF {PHP.usr.id} > 0 -->
	<!-- IF {PHP.usr.auth_write} -->
	<div class="uk-margin-top uk-text-center">
		<a class="uk-button uk-button-success uk-button-large" href="{PHP|cot_url('projects', 'm=add')}">{PHP.L.projects_add_to_catalog}</a>
	</div>
	<!-- ENDIF -->
	<!-- ENDIF -->
</div>
<div class="uk-container uk-container-center uk-text-center uk-margin-top uk-margin-bottom">
	<hr />
	<h2 class="uk-text-bold uk-text-danger">{PHP.L.title_freelancer_index_block}</h2>
	<div class="uk-grid uk-grid-margin-large" data-uk-grid-match="{target:'.uk-panel'}" data-uk-grid-margin="">
		<div class="uk-width-medium-1-4 uk-row-first" data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:300, repeat: true}">
			<a class="uk-panel uk-panel-box uk-panel-box-primary" href="#index-market-cats" data-uk-smooth-scroll="{offset: 90}">
				<div>
					<img src="themes/{PHP.theme}/img/land/sprite-icon-1.png" height="" width="" alt="{PHP.cfg.maintitle}">
				</div>
				<h2>{PHP.L.1st_title_freelancer_index_block}</h2>
				<p>{PHP.L.1st_descript_freelancer_index_block}</p>
			</a>
		</div>

		<div class="uk-width-medium-1-4" data-uk-scrollspy="{cls:'uk-animation-slide-top', delay:300, repeat: true}">
			<a class="uk-panel uk-panel-box uk-panel-box-primary" href="">
				<div>
					<img src="themes/{PHP.theme}/img/land/sprite-icon-2.png" height="" width="" alt="{PHP.cfg.maintitle}">
					</div>
				<h2>{PHP.L.2nd_title_freelancer_index_block}</h2>
				<p>{PHP.L.2nd_descript_freelancer_index_block}</p>
			</a>
		</div>

		<div class="uk-width-medium-1-4" data-uk-scrollspy="{cls:'uk-animation-slide-bottom', delay:600, repeat: true}">
			<a class="uk-panel uk-panel-box uk-panel-box-primary" href="">
				<div>
					<img src="themes/{PHP.theme}/img/land/sprite-icon-3.png" height="" width="" alt="{PHP.cfg.maintitle}">
				</div>
				<h2>{PHP.L.3rd_title_freelancer_index_block}</h2>
				<p>{PHP.L.3rd_descript_freelancer_index_block}</p>
			</a>
		</div>
		
		<div class="uk-width-medium-1-4" data-uk-scrollspy="{cls:'uk-animation-slide-right', delay:900, repeat: true}">
			<a class="uk-panel uk-panel-box uk-panel-box-primary" href="">
				<div>
					<img src="themes/{PHP.theme}/img/land/sprite-icon-4.png" height="" width="" alt="{PHP.cfg.maintitle}">
				</div>
				<h2>{PHP.L.4th_title_freelancer_index_block}</h2>
				<p>{PHP.L.4th_descript_freelancer_index_block}</p>
			</a>
		</div>
	</div>
	<!-- IF {PHP.usr.id} == 0 -->
	<div class="uk-margin-top uk-text-center">
		<a class="uk-button uk-button-primary uk-button-large" data-uk-modal="" href="#log">{PHP.L.market_add_product}</a>
	</div>
	<!-- ENDIF -->
	<!-- IF {PHP.usr.id} > 0 -->
	<!-- IF {PHP.usr.auth_write} -->
	<div class="uk-margin-top uk-text-center">
		<a class="uk-button uk-button-success uk-button-large" href="{PHP|cot_url('market', 'm=add')}">{PHP.L.market_add_product}</a>
	</div>
	<!-- ENDIF -->
	<!-- ENDIF -->
</div>