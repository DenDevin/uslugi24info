<!-- РАЗДЕЛ ЗАКАЗОВ -->
        <div id="cats" class="uk-container uk-container-center uk-margin-large-top uk-margin-large-bottom">
            <div class="uk-text-center">
                <h2><span class="tp-text-transform uk-text-bold tp-text-warning">Заявки и заказы</span> <span class="tp-text-transform uk-text-bold">ПОПУЛЯРНЫЕ КАТЕГОРИИ</span></h2>
                
            </div>
            <div class="uk-grid uk-grid-small" data-uk-grid-margin="">
                <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-4">
					<a class="uk-button uk-button-large uk-button-warning uk-width-1-1 uk-margin-bottom-remove" href="{PHP.L.Url_parent_category_1}" data-uk-tooltip="{pos:'top'}" title="Смотреть все подкатегории<br />«{PHP.L.Name_parent_category_1}»">{PHP.L.Name_parent_category_1}</a>
					<figure class="uk-overlay uk-overlay-hover">
						<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/index-projects-cats/01-programmirovanie.png" width="600" height="400" alt="">
						<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
								<ul class="uk-list uk-list-line">
									<li><a href="{PHP.L.Url_sub_category_1_1}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_1_1}</a></li>
									<li><a href="{PHP.L.Url_sub_category_1_2}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_1_2}</a></li>
									<li><a href="{PHP.L.Url_sub_category_1_3}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_1_3}</a></li>
									<li><a href="{PHP.L.Url_sub_category_1_4}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_1_4}</a></li>
								</ul>
						</figcaption>
					</figure>
                </div>
                <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-4">
					<a class="uk-button uk-button-large uk-button-warning uk-width-1-1 uk-margin-bottom-remove" href="{PHP.L.Url_parent_category_2}" data-uk-tooltip="{pos:'top'}" title="Смотреть все подкатегории<br />«{PHP.L.Name_parent_category_2}»">{PHP.L.Name_parent_category_2}</a>
					<figure class="uk-overlay uk-overlay-hover">
						<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/index-projects-cats/02-management.png" width="600" height="400" alt="">
						<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
								<ul class="uk-list uk-list-line">
									<li><a href="{PHP.L.Url_sub_category_2_1}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_2_1}</a></li>
									<li><a href="{PHP.L.Url_sub_category_2_2}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_2_2}</a></li>
									<li><a href="{PHP.L.Url_sub_category_2_3}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_2_3}</a></li>
									<li><a href="{PHP.L.Url_sub_category_2_4}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_2_4}</a></li>
								</ul>
						</figcaption>
					</figure>
                </div>
                <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-4">
					<a class="uk-button uk-button-large uk-button-warning uk-width-1-1 uk-margin-bottom-remove" href="{PHP.L.Url_parent_category_3}" data-uk-tooltip="{pos:'top'}" title="Смотреть все подкатегории<br />«{PHP.L.Name_parent_category_3}»">{PHP.L.Name_parent_category_3}</a>
					<figure class="uk-overlay uk-overlay-hover">
						<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/index-projects-cats/03-marketing.png" width="600" height="400" alt="">
						<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
								<ul class="uk-list uk-list-line">
									<li><a href="{PHP.L.Url_sub_category_3_1}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_3_1}</a></li>
									<li><a href="{PHP.L.Url_sub_category_3_2}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_3_2}</a></li>
									<li><a href="{PHP.L.Url_sub_category_3_3}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_3_3}</a></li>
									<li><a href="{PHP.L.Url_sub_category_3_4}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_3_4}</a></li>
								</ul>
						</figcaption>
					</figure>
                </div>
                <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-4">
					<a class="uk-button uk-button-large uk-button-warning uk-width-1-1 uk-margin-bottom-remove" href="{PHP.L.Url_parent_category_4}" data-uk-tooltip="{pos:'top'}" title="Смотреть все подкатегории<br />«{PHP.L.Name_parent_category_4}»">{PHP.L.Name_parent_category_4}</a>
					<figure class="uk-overlay uk-overlay-hover">
						<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/index-projects-cats/04-design.png" width="600" height="400" alt="">
						<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
								<ul class="uk-list uk-list-line">
									<li><a href="{PHP.L.Url_sub_category_4_1}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_4_1}</a></li>
									<li><a href="{PHP.L.Url_sub_category_4_2}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_4_2}</a></li>
									<li><a href="{PHP.L.Url_sub_category_4_3}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_4_3}</a></li>
									<li><a href="{PHP.L.Url_sub_category_4_4}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_4_4}</a></li>
								</ul>
						</figcaption>
					</figure>
                </div>
                <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-4">
					<a class="uk-button uk-button-large uk-button-warning uk-width-1-1 uk-margin-bottom-remove" href="{PHP.L.Url_parent_category_5}" data-uk-tooltip="{pos:'top'}" title="Смотреть все подкатегории<br />«{PHP.L.Name_parent_category_5}»">{PHP.L.Name_parent_category_5}</a>
					<figure class="uk-overlay uk-overlay-hover">
						<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/index-projects-cats/05-seo.png" width="600" height="400" alt="">
						<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
								<ul class="uk-list uk-list-line">
									<li><a href="{PHP.L.Url_sub_category_5_1}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_5_1}</a></li>
									<li><a href="{PHP.L.Url_sub_category_5_2}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_5_2}</a></li>
									<li><a href="{PHP.L.Url_sub_category_5_3}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_5_3}</a></li>
									<li><a href="{PHP.L.Url_sub_category_5_4}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_5_4}</a></li>
								</ul>
						</figcaption>
					</figure>
                </div>
                <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-4">
					<a class="uk-button uk-button-large uk-button-warning uk-width-1-1 uk-margin-bottom-remove" href="{PHP.L.Url_parent_category_6}" data-uk-tooltip="{pos:'top'}" title="Смотреть все подкатегории<br />«{PHP.L.Name_parent_category_6}»">{PHP.L.Name_parent_category_6}</a>
					<figure class="uk-overlay uk-overlay-hover">
						<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/index-projects-cats/06-text.png" width="600" height="400" alt="">
						<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
								<ul class="uk-list uk-list-line">
									<li><a href="{PHP.L.Url_sub_category_6_1}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_6_1}</a></li>
									<li><a href="{PHP.L.Url_sub_category_6_2}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_6_2}</a></li>
									<li><a href="{PHP.L.Url_sub_category_6_3}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_6_3}</a></li>
									<li><a href="{PHP.L.Url_sub_category_6_4}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_6_4}</a></li>
								</ul>
						</figcaption>
					</figure>
                </div>
                <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-4">
					<a class="uk-button uk-button-large uk-button-warning uk-width-1-1 uk-margin-bottom-remove" href="{PHP.L.Url_parent_category_7}" data-uk-tooltip="{pos:'top'}" title="Смотреть все подкатегории<br />«{PHP.L.Name_parent_category_7}»">{PHP.L.Name_parent_category_7}</a>
					<figure class="uk-overlay uk-overlay-hover">
						<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/index-projects-cats/07-engineering.png" width="600" height="400" alt="">
						<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
								<ul class="uk-list uk-list-line">
									<li><a href="{PHP.L.Url_sub_category_7_1}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_7_1}</a></li>
									<li><a href="{PHP.L.Url_sub_category_7_2}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_7_2}</a></li>
									<li><a href="{PHP.L.Url_sub_category_7_3}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_7_3}</a></li>
									<li><a href="{PHP.L.Url_sub_category_7_4}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_7_4}</a></li>
								</ul>
						</figcaption>
					</figure>
                </div>
                <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-4">
					<a class="uk-button uk-button-large uk-button-warning uk-width-1-1 uk-margin-bottom-remove" href="{PHP.L.Url_parent_category_8}" data-uk-tooltip="{pos:'top'}" title="Смотреть все подкатегории<br />«{PHP.L.Name_parent_category_8}»">{PHP.L.Name_parent_category_8}</a>
					<figure class="uk-overlay uk-overlay-hover">
						<img class="uk-overlay-spin" src="themes/{PHP.theme}/img/index-projects-cats/08-video-audio.png" width="600" height="400" alt="">
						<figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
								<ul class="uk-list uk-list-line">
									<li><a href="{PHP.L.Url_sub_category_8_1}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_8_1}</a></li>
									<li><a href="{PHP.L.Url_sub_category_8_2}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_8_2}</a></li>
									<li><a href="{PHP.L.Url_sub_category_8_3}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_8_3}</a></li>
									<li><a href="{PHP.L.Url_sub_category_8_4}" class="tp-text-transform uk-text-contrast">
										<i class="uk-margin-small-right uk-icon-caret-right"></i>{PHP.L.Name_sub_category_8_4}</a></li>
								</ul>
						</figcaption>
					</figure>
                </div>
            </div>
        </div>


<!-- РАЗДЕЛ ЗАКАЗОВ КОНЕЦ--> 