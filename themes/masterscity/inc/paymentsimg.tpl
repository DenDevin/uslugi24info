<div class="uk-slidenav-position" data-uk-slider="{center:true}">

    <div class="uk-slider-container">
        <ul class="uk-slider uk-grid ">
			<li class="uk-width-1-5" data-slider-slide="0" style=""><a href="https://www.free-kassa.ru/"><img src="https://www.free-kassa.ru/img/fk_btn/21.png"></a></li>
			<li class="uk-width-1-5" data-slider-slide="0" style=""><img src="themes/masterscity/img/payments/interkassa.png" alt="" draggable="false" data-holder-rendered="true" width="140" height="100"></li>
			<li class="uk-width-1-5" data-slider-slide="0" style=""><img src="themes/masterscity/img/payments/qiwi-wallet.png" alt="" draggable="false" data-holder-rendered="true" width="140" height="100"></li>
			<li class="uk-width-1-5" data-slider-slide="0" style=""><img src="themes/masterscity/img/payments/visa-mastercard.png" alt="" draggable="false" data-holder-rendered="true" width="140" height="100"></li>
			<li class="uk-width-1-5" data-slider-slide="0" style=""><img src="themes/masterscity/img/payments/ppcom.png" alt="" draggable="false" data-holder-rendered="true" width="140" height="100"></li>
			
        </ul>
    </div>

    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slider-item="previous"></a>
    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slider-item="next"></a>
</div>
          
