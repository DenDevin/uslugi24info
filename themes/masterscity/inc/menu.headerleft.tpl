<li><a href="/support-info/kak-zakazat-uslugu"><i class="uk-icon-chevron-right uk-margin-right"></i> Как заказать услугу?</a></li>
<li><a href="/support-info/kak-stat-ispolnitelem"><i class="uk-icon-chevron-right uk-margin-right"></i> Как стать исполнителем</a></li>
<li><a href="/support-info/sdelka-bezopasno"><i class="uk-icon-chevron-right uk-margin-right"></i> «Безопасная сделка»</a></li>
<li><a href="/support-info/partnerka"><i class="uk-icon-chevron-right uk-margin-right"></i> Партнерская программа</a></li>
<li><a href="/support-info/price"><i class="uk-icon-chevron-right uk-margin-right"></i> Платные услуги сайта</a></li>
<li><a href="{PHP|cot_url('contact')}"><i class="uk-icon-chevron-right uk-margin-right"></i> Контакты</a></li>
