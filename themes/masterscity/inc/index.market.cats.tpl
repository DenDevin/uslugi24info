        <div class="uk-container uk-container-center uk-margin-large-top uk-margin-large-bottom" id="index-market-cats">
            <div class="uk-text-center">
                <h2><span class="tp-text-transform uk-text-bold tp-text-warning">Витрина товаров и услуг</span><br /><span class="tp-text-transform uk-text-bold">основные категории</span></h2>
                
            </div>
            <div class="uk-grid uk-grid-small" data-uk-grid-margin="">
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=proektirovanie')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Проектирование»">
                        <div class="uk-panel-body">
							<i class="uk-icon-arrows-alt uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Проектирование</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=sborka-mebeli')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Сборка мебели»">
                        <div class="uk-panel-body">
							<i class="uk-icon-bed uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Сборка мебели</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=kontekstnaya-reklama')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Контекстная реклама»">
                        <div class="uk-panel-body">
							<i class="uk-icon-line-chart uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Контекстная реклама</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=gruzoperevozki-po-rossii')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Грузоперевозки по России»">
                        <div class="uk-panel-body">
							<i class="uk-icon-truck uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Грузоперевозки по России</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=kurerskie-uslugi')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Курьерские услуги»">
                        <div class="uk-panel-body">
							<i class="uk-icon-bicycle uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Курьерские услуги</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=montazh-d-p')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Монтаж детских площадок»">
                        <div class="uk-panel-body">
							<i class="uk-icon-cubes uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Монтаж детских площадок</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=derevoobrabotka')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Деревообработка»">
                        <div class="uk-panel-body">
							<i class="uk-icon-tree uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Деревообработка</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=yuridicheskie-uslugi')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Юридические услуги»">
                        <div class="uk-panel-body">
							<i class="uk-icon-balance-scale uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Юридические услуги</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=3d-proektirovanie')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«3D Проектирование»">
                        <div class="uk-panel-body">
							<i class="uk-icon-cube uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">3D Проектирование</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=sozdanie-saytov')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Создание веб-сайтов»">
                        <div class="uk-panel-body">
							<i class="uk-icon-code uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Создание веб-сайтов</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=nalogovaya-i-buhuchet')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Налоговая и бухучет»">
                        <div class="uk-panel-body">
							<i class="uk-icon-pie-chart uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Налоговая и бухучет</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=personalnyy-pomoshchnik')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Персональный помощник»">
                        <div class="uk-panel-body">
							<i class="uk-icon-user-plus uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Персональный помощник</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=dizaynerskie-uslugi')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Дизайнерские услуги»">
                        <div class="uk-panel-body">
							<i class="uk-icon-diamond uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Дизайнерские услуги</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=materialy-i-komplektuyushchie')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Материалы и комплектующие»">
                        <div class="uk-panel-body">
							<i class="uk-icon-puzzle-piece uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Материалы и комплектующие</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=bazy-dannyh-klientov')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Базы данных клиентов»">
                        <div class="uk-panel-body">
							<i class="uk-icon-users uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Базы данных клиентов</span></h5>
                        </div>
                    </a>
                </div>
                <div class="uk-width-medium-1-2 uk-width-medium-1-4 uk-text-center">
                    <a class="uk-padding-remove tp-panel uk-panel uk-panel-box uk-panel-box-hover tp-card-default tp-card-hover" href="{PHP|cot_url('market', 'c=menedzher-proektov')}" data-uk-tooltip="{pos:'top'}" title="Предложения услуг в категории<br />«Менеджер проектов»">
                        <div class="uk-panel-body">
							<i class="uk-icon-hand-pointer-o uk-icon-medium uk-text-black"></i>
							<h5><span class="uk-text-black tp-text-transform uk-text-bold">Менеджер проектов</span></h5>
                        </div>
                    </a>
                </div>					
            </div>
        </div>