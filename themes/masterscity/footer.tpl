<!-- BEGIN: FOOTER -->
<!-- IF {PHP.env.ext} == 'users' ! {PHP.m} == 'passrecover' -->    
<!-- IF {PHP.env.ext} == 'users' ! {PHP.m} == 'register' -->
<!-- IF {PHP._SERVER.REQUEST_URI|substr($this, 0, 6)} != '/login --> 
<footer class="uk-block uk-block-secondary uk-contrast">
    <div class="uk-container uk-container-center">
        <div class="uk-grid" data-uk-grid-margin="">
            
            
            
			<!-- IF {PHP.out.whosonline} -->
			<div class="uk-width-medium-1-4">
				<h5 class="online">{PHP.L.Online}</h2>
				<a href="{PHP|cot_url('plug','e=whosonline')}">{PHP.out.whosonline}</a>
				<!-- IF {PHP.out.whosonline_reg_list} -->:<br />{PHP.out.whosonline_reg_list}<!-- ENDIF -->
			</div>
			<!-- ENDIF -->
            
            
            
            <div class="uk-width-medium-1-4">
                <nav class="tp-footer-list">
                    <h5>Новым пользователям</h5>
                    <ul class="uk-list uk-list-line">
                        <li><i class="uk-icon-angle-double-right"></i>&nbsp;<a href="/support-info/about-service">{PHP.L.Useful_links_url1}</a></li>
                        <li><i class="uk-icon-angle-double-right"></i>&nbsp;<a href="/support-info/kak-zakazat-uslugu">{PHP.L.Useful_links_url2}</a></li>

                        <li><i class="uk-icon-angle-double-right"></i>&nbsp;<a href="/support-info/kak-stat-ispolnitelem">{PHP.L.Useful_links_url4}</a></li>	
						<li><i class="uk-icon-angle-double-right"></i>&nbsp;<a href="/support-info/sdelka-bezopasno">«Безопасная сделка»</a></li>
						<!-- IF {PHP.cot_plugins_active.affiliate} -->
						<li><i class="uk-icon-users"></i>&nbsp;<a href="/support-info/partnerka" data-uk-tooltip="{pos:'top'}" title="Как работает партнерская программа на сайте - {PHP.cfg.maintitle}">{PHP.L.affiliate}</a></li>
						<!-- ENDIF -->
                    </ul>
                </nav>
            </div>
            <div class="uk-width-medium-1-4">
                <nav class="tp-footer-list">
                    <h5>{PHP.L.Useful_links_title}</h5>
                    <ul class="uk-list uk-list-line">
						<li><i class="uk-icon-angle-double-right"></i>&nbsp;<a href="/dogovor-oferta">Договор-Оферта</a></li>
						<li><i class="uk-icon-angle-double-right"></i>&nbsp;<a href="/support-info/privacy-policy">Политика конфиденциальности</a></li>
                        <!-- IF {PHP.cot_plugins_active.useragreement} -->
                        <li><i class="uk-icon-angle-double-right"></i>&nbsp;<a href="{PHP|cot_url('useragreement')}">{PHP.L.Useragreement_title}</a></li>
                        <!-- ENDIF -->
						<li><i class="uk-icon-angle-double-right"></i>&nbsp;<a href="/support-info/price">Платные услуги сайта</a></li>
                        <!-- IF {PHP.cot_plugins_active.contact} -->
                        <li><i class="uk-icon-angle-double-right"></i>&nbsp;<a href="{PHP|cot_url('contact')}">{PHP.L.Contacty_title}</a></li>
                        <!-- ENDIF -->
                    </ul>
                </nav>
            </div>
            <div class="uk-width-medium-1-4 uk-position-relative">
                <nav>
                    <h5>Контакты и Техподержка</h5>
                    <ul class="uk-list uk-list-line">
        <!-- IF {PHP.usr.id} == 0 -->
        
        <div id="logtosupport" class="uk-modal">
            <div class="tp-modal-dialog-rigis uk-modal-dialog uk-animation-scale-up">
                <div class="uk-text-center uk-margin-bottom">
                    <a href="{PHP.cfg.mainurl}"><img src="themes/{PHP.theme}/img/log.png" height="40" width="140" alt="{PHP.cfg.maintitle}"></a>
                </div>
                <ul class="uk-list uk-margin-bottom">
                    <li class="uk-text-primary uk-text-break uk-text-bold">1. Вопросы в техподдержку и помощь доступны зарегистрированным пользователям.</li>
                    <li class="uk-text-primary uk-text-bold">2. Если Ваш вопрос общего характера и Вы не желаете регистрироваться, то воспользуйтесь общей формой  <a class="tp-color-ts tp-text-transform" href="{PHP|cot_url('contact')}">Обратной связи</a></li>
                    <li class="uk-text-primary uk-text-bold">3. Вы можете войти на сайт, через форму ниже, - такие запросы обрабатываются в приоритетном порядке.</li>
                </ul>
                <!-- IF !{PHP.msg} -->
                <form class="uk-form" action="login?a=check" method="post">
                    <div class="uk-form-row">
                        <div class="uk-form-icon">
                            <i class="uk-icon-user"></i>
                            <input class="uk-form-success uk-form uk-width-1-1" name="rusername" type="text" required="" placeholder="{PHP.L.users_nameormail}">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <div class="uk-form-icon">
                            <i class="uk-icon-unlock-alt"></i>
                            <input class="uk-form-success uk-form uk-width-1-1" name="rpassword" type="password" required="" placeholder="{PHP.L.Password}">
                        </div>
                    </div>

                    <button class="uk-margin-top tp-text-transform uk-button uk-button-primary uk-button-large uk-width-1-1" type="">{PHP.L.Login}</button>
                    <div class="uk-modal-footer">
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-2">
                                <a class="tp-color-ts" href="{PHP|cot_url('users','m=register')}">{PHP.L.Register}</a>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <a class="tp-color-ts" href="{PHP|cot_url('users', 'm=passrecover&a=request')}">{PHP.L.users_lostpass}?</a>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- ENDIF --> 
            </div>
        </div>

							<li><i class="uk-icon-cogs tp-icon-success uk-margin-right"></i><a data-uk-modal="" href="#logtosupport">Техподдержка и помощь</a></li>
							<!-- ELSE -->
                           <!-- IF {PHP.cot_plugins_active.support} -->
                           <li><i class="uk-icon-cogs tp-icon-success uk-margin-right"></i><a href="{PHP|cot_url('support')}" title="">Техподдержка и помощь</a></li>
                           <!-- ENDIF -->
		<!-- ENDIF -->
                        <!-- IF {PHP.cot_plugins_active.contact} -->
                        <li><i class="uk-icon-share-square-o tp-icon-success uk-margin-right"></i><a href="{PHP|cot_url('contact')}">Обратная связь</a></li>
                        <!-- ENDIF -->
                        <li>
                        <i class="uk-icon-phone tp-icon-success uk-margin-right"></i>{PHP.cfg.freetext2} 
                        </li>
                        <li>
                        <i class="uk-icon-phone tp-icon-success uk-margin-right"></i>{PHP.cfg.freetext3} 
                        </li>
                        <li>
                        <i class="uk-icon-envelope-o tp-icon-success uk-margin-right"></i>{PHP.cfg.adminemail}   
                        </li>
                        <li>
                        <i class="uk-icon-envelope-o tp-icon-success uk-margin-right"></i>{PHP.cfg.freetext1}   
                        </li>
                        <li>
                        <i class="uk-icon-university tp-icon-success uk-margin-right"></i>{PHP.cfg.freetext4}   
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
<!-- РАЗДЕЛ слайд лого платежных систем -->
{FILE "themes/{PHP.theme}/inc/paymentsimg.tpl"}
<!-- РАЗДЕЛ слайд лого платежных систем  КОНЕЦ--> 
    </div>
<!-- <a class="tp-add uk-hidden-small" href="{PHP|cot_url('contact')}" alt="{PHP.cfg.maintitle}"  data-uk-tooltip="{pos:'top'}" title="Купить готовый сайт услуг {PHP.cfg.maintitle} - {PHP.cfg.subtitle}"><i class="uk-icon-cart-arrow-down"></i></a> -->
 </footer>
<div class="tp-footer-bottom">
    <div class="uk-container uk-container-center" data-uk-scrollspy="{cls:'uk-animation-slide-left', repeat: true}">
	<div class="uk-grid" data-uk-grid-margin>
	<div class="uk-width-medium-1-3 uk-text-center">

        <div class="uk-navbar-content">

        </div>

    </div>
     
        <div class="uk-width-medium-1-3">
	<!-- IF {PHP.cot_plugins_active.useragreement} -->
        <div class="uk-navbar-content">
            <a target="_blank" class="uk-text-contrast uk-text-center" data-uk-tooltip="{pos:'top'}" title="{PHP.L.Useragreement_title}" href="{PHP|cot_url('useragreement')}">
                <i class="uk-icon-unlock-alt uk-icon-small uk-margin-right tp-icon-success"></i>{PHP.L.Useragreement_title}
            </a>
        </div>
	<!-- ENDIF -->
    </div>
    <div class="uk-width-medium-1-3">
        <div class="uk-navbar-content">
            <a target="_blank" class="uk-text-contrast uk-text-center" data-uk-tooltip="{pos:'top'}" title="Правила конфиденциальности" href="/support-info/privacy-policy"><i class="uk-icon-user-secret uk-icon-small uk-margin-right tp-icon-success"></i>Политика конфиденциальности</a>
        </div>
    </div>
</div>
</div>
</div>
<!-- ENDIF -->
<!-- ENDIF -->
<!-- ENDIF -->
<!-- <script>document.onkeydown=key;function key(){window.status=event.keyCode;if(event.keyCode==85&&event.ctrlKey)return false;if(event.keyCode==17)return false}window.onkeydown=function(evt){if(evt.keyCode==123)return false};window.onkeypress=function(evt){if(evt.keyCode==123)return false};document.ondragstart=test;document.onselectstart=test;document.oncontextmenu=test;function test(){return false}document.oncontextmenu;function catchControlKeys(event){var code=event.keyCode?event.keyCode:event.which?event.which:null;if(event.ctrlKey){if(code==117)return false;if(code==85)return false;if(code==99)return false;if(code==67)return false;if(code==97)return false;if(code==65)return false}}</script> -->

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
{FOOTER_RC} 

</body>
</html>
<!-- END: FOOTER --> 